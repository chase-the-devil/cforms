#include "CForms.h"
#include <stdio.h>
#include <iostream>

using namespace std;

TCApplication*	test;

TCWindow*		win;
TCWindow*		win1;

TCButton*		but;
TCLabel*		lb;
TCMemo*			mem;
TCListBox*		lst;
TCCheckBox*		chk;
TCSubMenu*		mn;
TCMainMenu*		mmn;

void Move()
	{
		but->DYNAMIC_REDRAW(but->GET_X(), but->GET_Y()+1, REDRAW, true);
		if(lst->GET_SELECTED_INFO())
			mem->ADD_TEXT(*lst->GET_SELECTED_INFO()->STRING);
	}

void main()
	{
		cout << sizeof(void*);
		char** LOGO = new char*[SCREEN_WIDTH];
		for(int yindex = 0; yindex < SCREEN_WIDTH; yindex++)
			{
				LOGO[yindex] = new char[SCREEN_HEIGHT];
				for(int xindex = 0; xindex < SCREEN_HEIGHT; xindex++)
					LOGO[yindex][xindex] = 177;
			}
		LOGO[9][9] = 'S';
		test =  new TCApplication(32, 219, LOGO, 1000);

		win = new TCWindow(100, 100, 0, 0, NULL);
		win1 = new TCWindow(60, 40, 0, 0, NULL);

		but = new TCButton(11, 4, 1, 1, NULL, 32, win);
		lb = new TCLabel(10, 5, 15, 1, 32, win);
		mem = new TCMemo(30, 30, 100, 100, 1, 1, NULL, win1);
		lst = new TCListBox(20, 20, 31, 1, NULL, win1);
		chk = new TCCheckBox(10, 2, 32, win);
		mn = new TCSubMenu(15, 14, 178, 32, mem);
		mmn = new TCMainMenu(C_LEFT, 3, 177);

		mmn->ADD_TO_END(mn, (char*)"Section 1", 176);
		mmn->ADD_TO_END(mn, (char*)"Section2", 176);
		mmn->ADD_TO_END(mn, (char*)"Section3", 176);
		mmn->ADD_TO_END(mn, (char*)"Section4", 176);
		mmn->ADD_TO_END(mn, (char*)"Section5", 176);
		mmn->ADD_TO_END(mn, (char*)"Section6", 176);
		mmn->ADD_TO_END(mn, (char*)"Section7", 176);
		mmn->ADD_TO_END(mn, (char*)"Section8", 176);
		mmn->ADD_TO_END(mn, (char*)"Section9", 176);
		mmn->ADD_TO_END(mn, (char*)"Section10", 176);
		mmn->ADD_TO_END(mn, (char*)"Section11", 176);
		mmn->ADD_TO_END(mn, (char*)"Section12", 176);
		mmn->ADD_TO_END(mn, (char*)"Section13", 176);
		mmn->ADD_TO_END(mn, (char*)"Section14", 176);
		for(int i = 0; i < 10; i++)
			{
				lst->ADD_TO_START(inttostr(i), 178, 176, 2);
				mn->PUSH(inttostr(i), 178, 177, 2);
			}
		mn->PUSH("Add text", 178, 177, 2, Move);
		but->SET_CAPTION("TCButton", 1, 1);
		lb->SET_CAPTION("TCLabel", 1, 1);
		test->RUN();
	}