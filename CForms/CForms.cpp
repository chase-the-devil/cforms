#include "CForms.h"


//********* VAR***********************************************************************************

TCApplication* hAPP				  = NULL;
HANDLE		   INPUT_CONSOLE	  = NULL;
HANDLE		   OUTPUT_CONSOLE	  = NULL;

//********* Functions*****************************************************************************


#ifdef _X86_

	int strtoint(char* input_str)
		{ 
			_asm	{
							mov ecx, 10
							lea ebx, [input_str]
							mov ebx, [ebx]

							cmp byte ptr[ebx], 45
							jne _next
							inc ebx

					  _next:
							mov eax, 0

					  _conv:
							cmp byte ptr[ebx], 0
							jz _fin
							
							
							mul ecx							

							mov dl, byte ptr[ebx]
							sub dl, 48
							add eax, edx

							inc ebx
							jmp _conv

					   _fin:
							lea ebx, [input_str]
							mov ebx, [ebx]
							cmp byte ptr[ebx], 45
							jne _end

							mov edx, eax
							mov eax, 0
							sub eax, edx

					   _end:
					}
		}

	char* inttostr(int input_num)
		{
			_asm	{
							mov edi, 10
							mov ecx, 0
							mov edx, 0

							mov eax, input_num
							cmp eax, 0
							jge _div

							mov ebx, 0
							sub ebx, eax
							mov eax, ebx
							inc ecx

							jmp _div

					   _len:
							cmp eax, 0
							jz  _mem
							
					   _div:
							div edi
							mov edx, 0

							inc ecx
							jmp _len

					   _mem:
							inc ecx
							push ecx
							call malloc
							pop ecx
							mov ebx, eax
							
							cmp input_num, 0
							jge _pcnv

							mov [ebx], byte ptr 45
							inc ebx
							dec ecx
							
				      _pcnv:
							mov eax, ecx
							dec eax

					   _off:
							inc ebx
							
							dec eax
							cmp eax, 0
							jnz _off

							mov [ebx], byte ptr 0

							mov eax, input_num
							cmp eax, 0
							jge _prep

							mov edx, 0
							sub edx, eax
							mov eax, edx

					  _prep:
							mov edx, 0
							jmp _divl

					   _cnv:
							cmp eax, 0
							jz  _fin

							mov edx, 0

					  _divl:
							cmp eax, 10
							jl _movl
						    
							sub eax, 10
							inc edx
							jmp _divl

					  _movl:
							add al, 48
							dec ebx
							mov [ebx], al

							mov eax, edx
							jmp _cnv

					   _fin:
							mov eax, ebx
							cmp input_num, 0
							jge  _end

							dec eax

					   _end:
					}
		}

#else

	int strtoint(char* input_str)
		{
			int ret = 0;
			if(*input_str == 45)
				{
					input_str++;
					while (*input_str)
						ret=((ret*10)-*(input_str++)+48);
				}
			else
				{
					while(*input_str)
						ret=((ret*10)+*(input_str++)-48);
				}
			return ret;
		}

	char* inttostr(int input_num)
		{
			register int buf, len = 2;
			char* ret;
			buf = input_num;
			if(buf < 0)
				{
					len++;
					while(buf <= -10)
						{
							buf/=10;
							len++;
						}
					ret = new char[len];
					*ret++ = 45;
					ret += len-2;
					*ret = 0;
					do
						{
							*(--ret) = -(input_num%10-48);
							input_num /= 10;
						}
					while(input_num != 0);
					ret--;
				}
			else
				{
					while(buf >= 10)
						{
							buf/=10;
							len++;
						}
					ret = new char[len];
					ret += len-1;
					*ret = 0;
					do
						{
							*(--ret) = input_num%10+48;
							input_num /= 10;
						}
					while(input_num != 0);
				}
			return ret;
		}

#endif

//************************************************************************************************
//******** TCScreen
//************************************************************************************************

TCScreen::TCScreen(char SYMBOL)
	{
		IN_DRAW = false;
		POS.X = 0;
		POS.Y = 0;
		for(register unsigned yindex = 0; yindex < SCREEN_HEIGHT; yindex++)
			{
				for(register unsigned xindex = 0; xindex < SCREEN_WIDTH; xindex++)
					{
						FAKE_CBUFFER[xindex][yindex] = new CPIXEL;
						FAKE_CBUFFER[xindex][yindex]->PREV_LAYER = NULL;
						FAKE_CBUFFER[xindex][yindex]->NEXT_LAYER = NULL;
						FAKE_CBUFFER[xindex][yindex]->hOWNER = NULL;
						FAKE_CBUFFER[xindex][yindex]->OWNER_CODE = 0;
						FAKE_CBUFFER[xindex][yindex]->OWNER_TYPE = 0;
						FAKE_CBUFFER[xindex][yindex]->PIX = SYMBOL;
					}
			}
	}

bool TCScreen::REDRAW()
	{
		if(!IN_DRAW)
			{
				IN_DRAW = true;
				CPIXEL* BUF_L;
				register int size = 0;
				for (register int yindex = 0; yindex < SCREEN_HEIGHT; yindex++)
					{
						for(register int xindex = 0; xindex < SCREEN_WIDTH; xindex++)
							{
								BUF_L = FAKE_CBUFFER[xindex][yindex];
								while(!BUF_L->PIX)
									BUF_L = BUF_L->PREV_LAYER;
								_LINE[size] = BUF_L->PIX;
								size++;
							}
					}
				WriteConsoleOutputCharacter(OUTPUT_CONSOLE, _LINE, SCREEN_WIDTH*SCREEN_HEIGHT, POS, &OFFSET);
				IN_DRAW = false;
				return true;
			}
		return false;
	}

//************************************************************************************************
//******** TCApplication
//************************************************************************************************

TCApplication::TCApplication(char SCR_SYMBOL_IN, char ARROW_SYMBOL, char** LOGO_SPRITE, int PAUSE)
	{
		if(!hAPP)
			{
				ARROW_STYLE = ARROW_SYMBOL;
				hAPP = this;
				OUTPUT_CONSOLE = GetStdHandle(STD_OUTPUT_HANDLE);
				INPUT_CONSOLE  = GetStdHandle(STD_INPUT_HANDLE);
				MAX_CODE = 2;
				MAIN_SCR = new TCScreen(SCR_SYMBOL_IN);
				for(register int yindex = 0; yindex < SCREEN_HEIGHT; yindex++)
					{
						for (register int xindex = 0; xindex < SCREEN_WIDTH; xindex++)
							LOGO[(yindex*(SCREEN_WIDTH))+xindex] = (TCHAR)(LOGO_SPRITE[xindex][yindex]);
					}
				LOGO_DELAY = PAUSE;
			}
	}

TCApplication::TCApplication(char SCR_SYMBOL_IN, char ARROW_SYMBOL)
	{
		if(!hAPP)
			{
				ARROW_STYLE = ARROW_SYMBOL;
				hAPP = this;
				OUTPUT_CONSOLE = GetStdHandle(STD_OUTPUT_HANDLE);
				INPUT_CONSOLE  = GetStdHandle(STD_INPUT_HANDLE);
				MAX_CODE = 2;
				MAIN_SCR = new TCScreen(SCR_SYMBOL_IN);
				for(register int yindex = 0; yindex < SCREEN_HEIGHT; yindex++)
					{
						for (register int index = 0; index < SCREEN_WIDTH; index++)
							LOGO[(yindex*(SCREEN_WIDTH))+index] = (TCHAR)178;		
					}
				LOGO_DELAY = 0;
			}
	}

TCApplication::TCApplication(char SCR_SYMBOL_IN, char** LOGO_SPRITE, int PAUSE)
	{
		if(!hAPP)
			{
				ARROW_STYLE = ARROW_PIX;
				hAPP = this;
				OUTPUT_CONSOLE = GetStdHandle(STD_OUTPUT_HANDLE);
				INPUT_CONSOLE  = GetStdHandle(STD_INPUT_HANDLE);
				MAX_CODE = 2;
				MAIN_SCR = new TCScreen(SCR_SYMBOL_IN);
				for(register int yindex = 0; yindex < SCREEN_HEIGHT; yindex++)
					{
						for (register int xindex = 0; xindex < SCREEN_WIDTH; xindex++)
							LOGO[(yindex*(SCREEN_WIDTH))+xindex] = (TCHAR)(LOGO_SPRITE[xindex][yindex]);			
					}
				LOGO_DELAY = PAUSE;
			}
	}
	
TCApplication::TCApplication(char SCR_SYMBOL_IN)
	{
		if(!hAPP)
			{
				ARROW_STYLE = ARROW_PIX;
				hAPP = this;
				OUTPUT_CONSOLE = GetStdHandle(STD_OUTPUT_HANDLE);
				INPUT_CONSOLE  = GetStdHandle(STD_INPUT_HANDLE);
				MAX_CODE = 2;
				MAIN_SCR = new TCScreen(SCR_SYMBOL_IN);
				for(register int yindex = 0; yindex < SCREEN_HEIGHT; yindex++)
					{
						for (register int index = 0; index < SCREEN_WIDTH; index++)
							LOGO[(yindex*(SCREEN_WIDTH-1))+index] = (TCHAR)178;		
					}
				LOGO_DELAY = 0;
			}
	}

TCApplication::~TCApplication()
	{
		if(OUTPUT_CONSOLE) 
			CloseHandle(OUTPUT_CONSOLE);
		if(INPUT_CONSOLE)
			CloseHandle(INPUT_CONSOLE);
		if(MAIN_SCR)
			delete MAIN_SCR;
		if(ARROW)
			delete ((TCArrow*)(ARROW));
		hAPP = NULL;
	}

unsigned  TCApplication::GET_MAX_CODE()
	{
		return MAX_CODE++;
	}

void TCApplication::RUN()
	{
		//Started flag
		STARTED = true;

		//IO console handles
		SetStdHandle(STD_OUTPUT_HANDLE, OUTPUT_CONSOLE);
		SetStdHandle(STD_INPUT_HANDLE, INPUT_CONSOLE);

		//Hiding console cursor
		GetConsoleCursorInfo(OUTPUT_CONSOLE, &(MAIN_SCR->CI));
		MAIN_SCR->CI.bVisible = false;
		SetConsoleCursorInfo(OUTPUT_CONSOLE, &(MAIN_SCR->CI));

		//Logo handling
		DWORD OFFSET;
		COORD POS;
		POS.X = 0;
		POS.Y = 0;
		WriteConsoleOutputCharacter(OUTPUT_CONSOLE, LOGO, SCREEN_WIDTH*SCREEN_HEIGHT, POS, &OFFSET);
		Sleep(LOGO_DELAY);

		//Console mode setup
		DWORD C_MODE, one, APP_MODE =  ENABLE_WINDOW_INPUT | ENABLE_MOUSE_INPUT | ENABLE_PROCESSED_INPUT;
		one = 1;

		GetConsoleMode(INPUT_CONSOLE, &C_MODE);
		SetConsoleMode(OUTPUT_CONSOLE, APP_MODE);

		//Console window size setup
		_CONSOLE_SCREEN_BUFFER_INFO TEMP_INFO;
		GetConsoleScreenBufferInfo(OUTPUT_CONSOLE, &TEMP_INFO);

		COORD REAL_BUFFER_SIZE;
		REAL_BUFFER_SIZE.X = SCREEN_WIDTH;
		REAL_BUFFER_SIZE.Y = SCREEN_HEIGHT;
	
		SMALL_RECT windowSize;

		if(SCREEN_WIDTH > TEMP_INFO.dwSize.X || SCREEN_HEIGHT > TEMP_INFO.dwSize.Y)
			{
				SetConsoleScreenBufferSize(OUTPUT_CONSOLE, REAL_BUFFER_SIZE);
				windowSize = { 0, 0, SCREEN_WIDTH-1, SCREEN_HEIGHT-1 };
				SetConsoleWindowInfo(OUTPUT_CONSOLE, TRUE, &windowSize);
			}
		else
			{
				windowSize = { 0, 0, SCREEN_WIDTH-1, SCREEN_HEIGHT-1 };
				SetConsoleWindowInfo(OUTPUT_CONSOLE, TRUE, &windowSize);
				SetConsoleScreenBufferSize(OUTPUT_CONSOLE, REAL_BUFFER_SIZE);
			}
		REAL_BUFFER_SIZE = TEMP_INFO.dwSize;

		//Arrow style setup
		if(ARROW_STYLE == ARROW_PIX)
			ARROW = new TCArrow;
		else
			ARROW = new TCArrow(ARROW_STYLE);

		//Cycle flag
		bool cycle = true;

		//Input message record
		INPUT_RECORD* EVENT_BUF = new INPUT_RECORD;
		
		//Drawing screen
		MAIN_SCR->REDRAW();

		//Main cycle(all input messages handler)
		while(cycle)
			{
				//Reading one system message
				ReadConsoleInput(INPUT_CONSOLE, EVENT_BUF, 1, &one);
				//Checking it
				switch (EVENT_BUF->EventType)
					{
						//if it was mouse event
						case MOUSE_EVENT:
							{
								//then check what is it
								switch (EVENT_BUF->Event.MouseEvent.dwEventFlags)
									{
										//if it was moved
										case MOUSE_MOVED:
											{
												//if there was some object sticked to mouse we have to move it as well
												if(((TCArrow*)(ARROW))->STICKED)
													//so we just making redrawing it by changing mouse coordinates
													((TCArrow*)(ARROW))->STICKED->DYNAMIC_REDRAW(EVENT_BUF->Event.MouseEvent.dwMousePosition.X - ((TCArrow*)(ARROW))->GET_X() + ((TCArrow*)(ARROW))->STICKED->GET_X(), EVENT_BUF->Event.MouseEvent.dwMousePosition.Y - ((TCArrow*)(ARROW))->GET_Y() +((TCArrow*)(ARROW))->STICKED->GET_Y(), REDRAW, false);
												else
													//else - we checking reactions of other objects under mouse arrow(if there was some of them) by calling TCArrow::REACT()
													((TCArrow*)(ARROW))->REACT(EVENT_BUF->Event.MouseEvent.dwMousePosition.X, EVENT_BUF->Event.MouseEvent.dwMousePosition.Y, C_MMOVE);
												//and we making redrawing of arrow cuz it was moved
												((TCArrow*)(ARROW))->DYNAMIC_REDRAW(EVENT_BUF->Event.MouseEvent.dwMousePosition.X, EVENT_BUF->Event.MouseEvent.dwMousePosition.Y, REDRAW, true);
												break;
											}
										//if there was clicking or releasing of mouse button
										case 0:
											{
												//if it was mouse1_click(left button)
												if (EVENT_BUF->Event.MouseEvent.dwButtonState == FROM_LEFT_1ST_BUTTON_PRESSED)
													{
														//saving wich button was pressed last time
														((TCArrow*)(ARROW))->LAST_PRESSED = FROM_LEFT_1ST_BUTTON_PRESSED;
														//calling reaction of objects on mouse down left event by calling TCArrow::REACT()		
														((TCArrow*)(ARROW))->REACT(((TCArrow*)(ARROW))->GET_X(), ((TCArrow*)(ARROW))->GET_Y(), C_MDOWNL);
														//calling reaction of objects on select event by calling TCArrow::REACT()
														((TCArrow*)(ARROW))->REACT(((TCArrow*)(ARROW))->GET_X(), ((TCArrow*)(ARROW))->GET_Y(), C_SELECT);
													}
												//if there was mouse2_click(right button)
												else if (EVENT_BUF->Event.MouseEvent.dwButtonState == RIGHTMOST_BUTTON_PRESSED)
													{
														//saving wich button was pressed last time
														((TCArrow*)(ARROW))->LAST_PRESSED = RIGHTMOST_BUTTON_PRESSED;
														//calling reaction of objects on mouse down right event by calling TCArrow::REACT()
														((TCArrow*)(ARROW))->REACT(((TCArrow*)(ARROW))->GET_X(), ((TCArrow*)(ARROW))->GET_Y(), C_MDOWNR);
													}
												//if some button was released
												else
													{
														//if some object got clicked before
														if(((TCArrow*)(ARROW))->CLICKED)
															{
																//calling reaction of clicked object on mouse up event
																((TCArrow*)(ARROW))->CLICKED->REACT(((TCArrow*)(ARROW))->GET_X(), ((TCArrow*)(ARROW))->GET_Y(), C_MUP);
																//clearing pointer on clicked object
																((TCArrow*)(ARROW))->CLICKED = NULL;
															}
														//else
														else
															//calling reaction of objects on mouse up event by calling TCArrow::REACT()
															((TCArrow*)(ARROW))->REACT(((TCArrow*)(ARROW))->GET_X(), ((TCArrow*)(ARROW))->GET_Y(), C_MUP);
														//clearing last pressed button on mouse, cuz we released it
														((TCArrow*)(ARROW))->LAST_PRESSED = NULL;	
													}
												break;
											}
										//if there was double click event
										case DOUBLE_CLICK:
											{
												//if it was left mouse button click
												if(EVENT_BUF->Event.MouseEvent.dwButtonState == FROM_LEFT_1ST_BUTTON_PRESSED)
													{
														//saving wich button was pressed last time
														((TCArrow*)(ARROW))->LAST_PRESSED = FROM_LEFT_1ST_BUTTON_PRESSED;
														//calling reaction of objects on mouse down left event by calling TCArrow::REACT()
														((TCArrow*)(ARROW))->REACT(((TCArrow*)(ARROW))->GET_X(), ((TCArrow*)(ARROW))->GET_Y(), C_MDOWNL);
													}
												//if it was right button, or else
												else
													{
														//saving wich button was pressed last time
														((TCArrow*)(ARROW))->LAST_PRESSED = RIGHTMOST_BUTTON_PRESSED;
														//calling reaction of objects on mouse down right event by calling TCArrow::REACT()
														((TCArrow*)(ARROW))->REACT(((TCArrow*)(ARROW))->GET_X(), ((TCArrow*)(ARROW))->GET_Y(), C_MDOWNR);
													}
												//calling reaction of objects on mouse double click event by calling TCArrow::REACT()
												((TCArrow*)(ARROW))->REACT(((TCArrow*)(ARROW))->GET_X(), ((TCArrow*)(ARROW))->GET_Y(), C_MDBCLICK);
												break;
											}
										//if it was mouse wheel event
										case MOUSE_WHEELED:
											{ 	
												//if it was scroll up
												if(EVENT_BUF->Event.MouseEvent.dwButtonState < SCROLL_DOWN)
													//calling reaction of objects on mouse wheel up event by calling TCArrow::REACT()
													((TCArrow*)(ARROW))->REACT(((TCArrow*)(ARROW))->GET_X(), ((TCArrow*)(ARROW))->GET_Y(), C_MSCROLL_UP);
												//else
												else
													//calling reaction of objects on mouse wheel down event by calling TCArrow::REACT()
													((TCArrow*)(ARROW))->REACT(((TCArrow*)(ARROW))->GET_X(), ((TCArrow*)(ARROW))->GET_Y(), C_MSCROLL_DOWN);
												break;
											}
									}
								break;
							}
						//if it was some key event
						case KEY_EVENT:
							{
								//if it was some key pressed
								if (EVENT_BUF->Event.KeyEvent.bKeyDown)
									{
										//saving virtual key code
										((TCArrow*)(ARROW))->INPUT_KEY = EVENT_BUF->Event.KeyEvent.wVirtualKeyCode; 
										//saving control keys state
										((TCArrow*)(ARROW))->CONTROL_KEY_STATE = EVENT_BUF->Event.KeyEvent.dwControlKeyState;
										//calling reaction of objects on key press event by calling TCArrow::REACT()
										((TCArrow*)(ARROW))->REACT(((TCArrow*)(ARROW))->GET_X(), ((TCArrow*)(ARROW))->GET_Y(), C_KEY_INPUT);
									}
								//if some key was released
								else
									{
										//if it was Esc key released
										if(EVENT_BUF->Event.KeyEvent.wVirtualKeyCode == 27)
											//exit from cycle
											cycle = false;
									}
								break;
							}
						//if was window buffer size event
						case WINDOW_BUFFER_SIZE_EVENT:
							//setting window buffer size to default
							SetConsoleWindowInfo(OUTPUT_CONSOLE, TRUE, &windowSize);
					}
			}

		//Old console screen size setup
		windowSize = {0, 0, REAL_BUFFER_SIZE.X, REAL_BUFFER_SIZE.Y};
		SetConsoleWindowInfo(OUTPUT_CONSOLE, TRUE, &windowSize);

		//Old console mode setup
		SetConsoleMode(OUTPUT_CONSOLE, C_MODE);

		//Making cursor visible
		MAIN_SCR->CI.bVisible = true;
		SetConsoleCursorInfo(OUTPUT_CONSOLE, &(MAIN_SCR->CI));

		//Terminate process flags setup
		TERMINATED = true;
		STARTED = false;

		//Undrawing arrow
		((TCArrow*)(ARROW))->DYNAMIC_REDRAW(0, 0, UNDRAW, false);

		//Sleeping till all threads will be closed
		Sleep(40);
	}

bool TCApplication::IN_PROCESS()
	{
		return STARTED;
	}

//************************************************************************************************
//******** TCObject
//************************************************************************************************

TCObject::TCObject(unsigned new_WIDTH, unsigned new_HEIGHT, int new_X, int new_Y, char in_BACKGROUND, unsigned in_TYPE)
	{
		CODE					= ((TCApplication*)(hAPP))->GET_MAX_CODE();
		TAG						= 0;
		ENABLED					= true;
		WIDTH					= new_WIDTH;
		HEIGHT					= new_HEIGHT;
		X						= new_X;
		Y						= new_Y;
		TYPE					= in_TYPE;
		BACKGROUND				= in_BACKGROUND;
		AVAILABLE_XMIN			= 0;
		AVAILABLE_YMIN			= 0;
		AVAILABLE_XMAX			= WIDTH;
		AVAILABLE_YMAX			= HEIGHT;
		MENU					= NULL;
		ACTION_ON_MOUSE_DOWN	= NULL;
		ACTION_ON_MOUSE_DOWN_R	= NULL;
		ACTION_ON_MOUSE_UP		= NULL;
		ACTION_ON_MOUSE_MOVE	= NULL;
		ACTION_ON_MOUSE_DBCLICK = NULL;
		ACTION_ON_SCROLL_UP		= NULL;
		ACTION_ON_SCROLL_DOWN	= NULL;
		ACTION_ON_SELECT		= NULL;
		ACTION_ON_UNSELECT		= NULL;
		if(WIDTH > 0 && HEIGHT > 0)
			{
				SPRITE = new CPIXEL**[WIDTH];
				for (register unsigned xindex = 0; xindex < WIDTH; xindex++)
					{
						SPRITE[xindex] = new CPIXEL*[HEIGHT];
						for (register unsigned yindex = 0; yindex < HEIGHT; yindex++)
							{
								SPRITE[xindex][yindex]		   = new CPIXEL;
								SPRITE[xindex][yindex]->hOWNER = NULL;
								SPRITE[xindex][yindex]->PIX    = in_BACKGROUND;
								SPRITE[xindex][yindex]->NEXT_LAYER = NULL;
								SPRITE[xindex][yindex]->PREV_LAYER = NULL;
								SPRITE[xindex][yindex]->OWNER_CODE = 0;
								SPRITE[xindex][yindex]->OWNER_TYPE = 0;
							}
					}
			}
		else
			SPRITE = NULL;
	}

TCObject::~TCObject()
	{
		if(SPRITE)
			{
				for(register unsigned xindex = 0; xindex < WIDTH; xindex++)
					{
						for(register unsigned yindex = 0; yindex < HEIGHT; yindex++)
							delete SPRITE[xindex][yindex];
						delete[] SPRITE[xindex];
					}
				delete[] SPRITE;
			}
	}

unsigned TCObject::GET_CODE()
	{
		return CODE;
	}

unsigned TCObject::GET_TYPE()
	{
		return TYPE;
	}

int	TCObject::GET_MAX_HEIGHT()
	{
		return MAX_HEIGHT;
	}

int	TCObject::GET_MAX_WIDTH()
	{
		return MAX_WIDTH;
	}

unsigned TCObject::GET_HEIGHT()
	{
		return HEIGHT;
	}

unsigned TCObject::GET_WIDTH()
	{
		return WIDTH;
	}

int TCObject::GET_X()
	{
		return X;
	}

int TCObject::GET_Y()
	{
		return Y;
	}

unsigned TCObject::WDH_HGH_RET(unsigned new_PARAM)
	{
		if(new_PARAM < 2)
			return 2;
		else
			return new_PARAM;
	}

bool TCObject::STATIC_REDRAW(bool is_UPDATE)
	{
		unsigned wdh, hgh, sx, sy, spx, spy;;
		CPIXEL*  BUF_L;
		if(X >= (int)(SCREEN_WIDTH) || Y >= (int)(SCREEN_HEIGHT) || -X > (int)(WIDTH) || -Y > (int)(HEIGHT))
			return false;
		if (X + WIDTH > SCREEN_WIDTH)
			wdh = SCREEN_WIDTH;
		else
			wdh = X + WIDTH;
		if (Y + HEIGHT > SCREEN_HEIGHT)
			hgh = SCREEN_HEIGHT;
		else
			hgh = Y + HEIGHT;
		if (X < 0)
			{
				sx = 0;
				spx = -X;
			}
		else
			{
				sx = X;
				spx = 0;
			}
		if (Y < 0)
			{
				sy = 0;
				spy = -Y;
			}
		else
			{
				sy = Y;
				spy = 0;
			}
		for(register unsigned xindex = sx; xindex < wdh; xindex++)
			{
				for(register unsigned yindex = sy; yindex < hgh; yindex++)
					{
						BUF_L = hAPP->MAIN_SCR->FAKE_CBUFFER[xindex][yindex];
						while (BUF_L)
							{
								if(BUF_L->OWNER_CODE == CODE)
									{
										BUF_L->PIX = SPRITE[xindex-sx+spx][yindex-sy+spy]->PIX;
										break;
									}
								else
									BUF_L = BUF_L->PREV_LAYER;
							}
					}
			}
		if(is_UPDATE)
			hAPP->MAIN_SCR->REDRAW();
		return true;
	}

bool TCObject::DYNAMIC_REDRAW(int new_X, int new_Y, DRAW_EVENT in_EVENT, bool is_UPDATE)
	{
		unsigned wdh, hgh, sx, sy, spx, spy;
		CPIXEL*  BUF_L;
		if(in_EVENT == UNDRAW || in_EVENT == REDRAW)
			{
				if(!(X >= (int)(SCREEN_WIDTH) || Y >= (int)(SCREEN_HEIGHT) || -X > (int)(WIDTH) || -Y > (int)(HEIGHT)))
					{
						if (X + WIDTH > SCREEN_WIDTH)
							wdh = SCREEN_WIDTH;
						else
							wdh = X + WIDTH;
						if (Y + HEIGHT > SCREEN_HEIGHT)
							hgh = SCREEN_HEIGHT;
						else
							hgh = Y + HEIGHT;
						if (X < 0)
							{
								sx = 0;
								spx = -X;
							}
						else
							{
								sx = X;
								spx = 0;
							}
						if (Y < 0)
							{
								sy = 0;
								spy = -Y;
							}
						else
							{
								sy = Y;
								spy = 0;
							}
						for(register unsigned xindex = sx; xindex < wdh; xindex++)
							{
								for(register unsigned yindex = sy; yindex < hgh; yindex++)
									{
										BUF_L = hAPP->MAIN_SCR->FAKE_CBUFFER[xindex][yindex];
										while (BUF_L)
											{
												if(BUF_L->OWNER_CODE == CODE)
													{
														if(BUF_L->NEXT_LAYER)
															{
																BUF_L->NEXT_LAYER->PREV_LAYER = BUF_L->PREV_LAYER;
																BUF_L->PREV_LAYER->NEXT_LAYER = BUF_L->NEXT_LAYER;
															}
														else if (BUF_L->PREV_LAYER)
															{
																hAPP->MAIN_SCR->FAKE_CBUFFER[xindex][yindex] = BUF_L->PREV_LAYER;
																hAPP->MAIN_SCR->FAKE_CBUFFER[xindex][yindex]->NEXT_LAYER = NULL;
															}
														delete BUF_L;
														break;
													}
												else
													BUF_L = BUF_L->PREV_LAYER;
											}
									}
							}
					}
			}
		if(in_EVENT == DRAW || in_EVENT == REDRAW)
			{
				X = new_X;
				Y = new_Y;
				if(X >= (int)(SCREEN_WIDTH) || Y >= (int)(SCREEN_HEIGHT) || -X > (int)(WIDTH) || -Y > (int)(HEIGHT))
					return false;
				if (X + WIDTH > SCREEN_WIDTH)
					wdh = SCREEN_WIDTH;
				else
					wdh = X + WIDTH;
				if (Y + HEIGHT > SCREEN_HEIGHT)
					hgh = SCREEN_HEIGHT;
				else
					hgh = Y + HEIGHT;
				if (X < 0)
					{
						sx = 0;
						spx = -X;
					}
				else
					{
						sx = X;
						spx = 0;
					}
				if (Y < 0)
					{
						sy = 0;
						spy = -Y;
					}
				else
					{
						sy = Y;
						spy = 0;
					}
				CPIXEL* BUF_L2;
				for(register unsigned xindex = sx; xindex < wdh; xindex++)
					{
						for(register unsigned yindex = sy; yindex < hgh; yindex++)
							{
								BUF_L													 = new CPIXEL;
								BUF_L->hOWNER											 = this;
								BUF_L->PIX												 = SPRITE[xindex-sx+spx][yindex-sy+spy]->PIX;
								BUF_L->NEXT_LAYER										 = NULL;
								BUF_L->OWNER_CODE										 = CODE;
								BUF_L->OWNER_TYPE										 = TYPE;
								if (hAPP->MAIN_SCR->FAKE_CBUFFER[xindex][yindex]->OWNER_TYPE == 1 || hAPP->MAIN_SCR->FAKE_CBUFFER[xindex][yindex]->OWNER_TYPE == 9)
									{
										BUF_L2 = hAPP->MAIN_SCR->FAKE_CBUFFER[xindex][yindex];
										while(BUF_L2->OWNER_TYPE == 1 || BUF_L2->OWNER_TYPE == 9)
											BUF_L2 = BUF_L2->PREV_LAYER;
										if(BUF_L2->NEXT_LAYER)
											BUF_L2->NEXT_LAYER->PREV_LAYER = BUF_L;
										BUF_L->NEXT_LAYER = BUF_L2->NEXT_LAYER;
										BUF_L->PREV_LAYER = BUF_L2;
										BUF_L2->NEXT_LAYER = BUF_L;
									}
								else
									{
										BUF_L->PREV_LAYER										 = hAPP->MAIN_SCR->FAKE_CBUFFER[xindex][yindex];
										hAPP->MAIN_SCR->FAKE_CBUFFER[xindex][yindex]->NEXT_LAYER = BUF_L;
										hAPP->MAIN_SCR->FAKE_CBUFFER[xindex][yindex]			 = BUF_L;
									}
							}
					}
				if(is_UPDATE)
					hAPP->MAIN_SCR->REDRAW();
				return true;
			}
		if (is_UPDATE)
			hAPP->MAIN_SCR->REDRAW();
		return true;
	}

//************************************************************************************************
//******** TCComponent
//************************************************************************************************

TCComponent::TCComponent(unsigned new_WIDTH, unsigned new_HEIGHT, int new_X, int new_Y, char in_BACKGROUND, unsigned in_TYPE, TCObject* in_PARENT) : TCObject(new_WIDTH, new_HEIGHT, new_X, new_Y, in_BACKGROUND, in_TYPE)
	{
		PARENT = in_PARENT;
	}

bool TCComponent::STATIC_REDRAW(bool is_UPDATE)
	{
		unsigned wdh, hgh, sx, sy, spx, spy;
		CPIXEL*  BUF_L;
		if(X >= PARENT->AVAILABLE_XMAX || Y >= PARENT->AVAILABLE_YMAX || -X+PARENT->AVAILABLE_XMIN > (int)(WIDTH) || -Y+PARENT->AVAILABLE_YMIN > (int)(HEIGHT))
			return false;
		if (X + (int)(WIDTH) > PARENT->AVAILABLE_XMAX)
			wdh = PARENT->AVAILABLE_XMAX;
		else
			wdh = X + WIDTH;
		if (Y + (int)(HEIGHT) > PARENT->AVAILABLE_YMAX)
			hgh = PARENT->AVAILABLE_YMAX;
		else
			hgh = Y + HEIGHT;
		if (X < PARENT->AVAILABLE_XMIN)
			{
				sx = PARENT->AVAILABLE_XMIN;
				spx = PARENT->AVAILABLE_XMIN-X;
			}
		else
			{
				sx = X;
				spx = 0;
			}
		if (Y < PARENT->AVAILABLE_YMIN)
			{
				sy = PARENT->AVAILABLE_YMIN;
				spy = PARENT->AVAILABLE_YMIN-Y;
			}
		else
			{
				sy = Y;
				spy = 0;
			}
		for(register unsigned xindex = sx; xindex < wdh; xindex++)
			{
				for(register unsigned yindex = sy; yindex < hgh; yindex++)
					{
						BUF_L = PARENT->SPRITE[xindex][yindex];
						while (BUF_L)
							{
								if(BUF_L->OWNER_CODE == CODE)
									{
										BUF_L->PIX = SPRITE[xindex-sx+spx][yindex-sy+spy]->PIX;
										if(!SPRITE[xindex-sx+spx][yindex-sy+spy]->PIX)
											{
												CPIXEL* BUF_P = BUF_L->PREV_LAYER;
												while(BUF_P)
													{
														if(BUF_P->PIX)
															{
																BUF_L->PIX = BUF_P->PIX;
																break;
															}
														BUF_P = BUF_P->PREV_LAYER;
													}
											}
										break;
									}
								else
									BUF_L = BUF_L->PREV_LAYER;
							}
					}
			}
		PARENT->STATIC_REDRAW(is_UPDATE);
		return true;
	}

bool TCComponent::DYNAMIC_REDRAW(int new_X, int new_Y, DRAW_EVENT in_EVENT, bool is_UPDATE)
	{
		unsigned wdh, hgh, sx, sy, spx, spy;
		CPIXEL*  BUF_L;
		if(in_EVENT == UNDRAW || in_EVENT == REDRAW)
			{
				if(!(X >= (int)(PARENT->GET_MAX_WIDTH()) || Y >= (int)(PARENT->GET_MAX_HEIGHT()) || -X > (int)(WIDTH) || -Y > (int)(HEIGHT)))
					{
						if (X + (int)(WIDTH) > PARENT->GET_MAX_WIDTH())
							wdh = PARENT->GET_MAX_WIDTH();
						else
							wdh = X + WIDTH;
						if (Y + (int)(HEIGHT) > PARENT->GET_MAX_HEIGHT())
							hgh = PARENT->GET_MAX_HEIGHT();
						else
							hgh = Y + HEIGHT;
						if (X < 0)
							{
								sx = 0;
								spx = -X;
							}
						else
							{
								sx = X;
								spx = 0;
							}
						if (Y < 0)
							{
								sy = 0;
								spy = -Y;
							}
						else
							{
								sy = Y;
								spy = 0;
							}
						for(register unsigned xindex = sx; xindex < wdh; xindex++)
							{
								for(register unsigned yindex = sy; yindex < hgh; yindex++)
									{
										BUF_L = PARENT->SPRITE[xindex][yindex];
										while (BUF_L)
											{
												if(BUF_L->OWNER_CODE == CODE)
													{
														if(BUF_L->NEXT_LAYER)
															{
																BUF_L->NEXT_LAYER->PREV_LAYER = BUF_L->PREV_LAYER;
																BUF_L->PREV_LAYER->NEXT_LAYER = BUF_L->NEXT_LAYER;
															}
														else if (BUF_L->PREV_LAYER)
															{
																PARENT->SPRITE[xindex][yindex] = BUF_L->PREV_LAYER;
																PARENT->SPRITE[xindex][yindex]->NEXT_LAYER = NULL;
																delete BUF_L;
															}
														break;
													}
												else
													BUF_L = BUF_L->PREV_LAYER;
											}
									}
							}
					}
			}
		if(in_EVENT == DRAW || in_EVENT == REDRAW)
			{
				X = new_X;
				Y = new_Y;
				if(X >= (int)(PARENT->GET_MAX_WIDTH()) || Y >= (int)(PARENT->GET_MAX_HEIGHT()) || -X+PARENT->AVAILABLE_XMIN > (int)(WIDTH) || -Y+PARENT->AVAILABLE_YMIN > (int)(HEIGHT))
					{
						if(is_UPDATE && in_EVENT == REDRAW)
							PARENT->DYNAMIC_REDRAW(PARENT->GET_X(), PARENT->GET_Y(), REDRAW, true);
						return false;
					}
				if (X + (int)(WIDTH) > (int)(PARENT->GET_MAX_WIDTH()))
					wdh = PARENT->AVAILABLE_XMAX;
				else
					wdh = X + WIDTH;
				if (Y + (int)(HEIGHT) > (int)(PARENT->GET_MAX_HEIGHT()))
					hgh = PARENT->AVAILABLE_YMAX;
				else
					hgh = Y + HEIGHT;
				if (X < PARENT->AVAILABLE_XMIN)
					{
						sx = PARENT->AVAILABLE_XMIN;
						spx = PARENT->AVAILABLE_XMIN-X;
					}
				else
					{
						sx = X;
						spx = 0;
					}
				if (Y < PARENT->AVAILABLE_YMIN)
					{
						sy = PARENT->AVAILABLE_YMIN;
						spy = PARENT->AVAILABLE_YMIN-Y;
					}
				else
					{
						sy = Y;
						spy = 0;
					}
				for(register unsigned xindex = sx; xindex < wdh; xindex++)
					{
						for(register unsigned yindex = sy; yindex < hgh; yindex++)
							{
								BUF_L													 = new CPIXEL;
								BUF_L->hOWNER											 = this;
								BUF_L->PIX												 = SPRITE[xindex-sx+spx][yindex-sy+spy]->PIX;
								if(!SPRITE[xindex-sx+spx][yindex-sy+spy]->PIX)
									{
										CPIXEL* BUF_P = PARENT->SPRITE[xindex][yindex];
										while(BUF_P)
											{
												if(BUF_P->PIX && BUF_P->OWNER_CODE != PARENT->GET_CODE())
													{
														BUF_L->PIX = BUF_P->PIX;
														break;
													}
												BUF_P = BUF_P->PREV_LAYER;
											}
									}
								BUF_L->NEXT_LAYER										 = NULL;
								BUF_L->OWNER_CODE										 = CODE;
								BUF_L->OWNER_TYPE										 = TYPE;
								if (PARENT->SPRITE[xindex][yindex]->OWNER_CODE == PARENT->GET_CODE())
									{
										BUF_L->PREV_LAYER = PARENT->SPRITE[xindex][yindex]->PREV_LAYER;
										PARENT->SPRITE[xindex][yindex]->PREV_LAYER->NEXT_LAYER = BUF_L;
										BUF_L->NEXT_LAYER = PARENT->SPRITE[xindex][yindex];
										PARENT->SPRITE[xindex][yindex]->PREV_LAYER = BUF_L;
									}
								else
									{
										BUF_L->PREV_LAYER										 = PARENT->SPRITE[xindex][yindex];
										PARENT->SPRITE[xindex][yindex]->NEXT_LAYER				 = BUF_L;
										PARENT->SPRITE[xindex][yindex]							 = BUF_L;
									}
							}
					}
				PARENT->DYNAMIC_REDRAW(PARENT->GET_X(), PARENT->GET_Y(), REDRAW, is_UPDATE);
				return true;
			}
		PARENT->DYNAMIC_REDRAW(PARENT->GET_X(), PARENT->GET_Y(), REDRAW, is_UPDATE);
		return true;
	}

TCObject* TCComponent::GET_PARENT()
	{
		return PARENT;
	}

//************************************************************************************************
//******** TCArrow
//************************************************************************************************

TCArrow::TCArrow() : TCObject(0, 0, 0, 0, (char)(ARROW_PIX), CARROW) 
	{
		DYNAMIC_REDRAW(0, 0, DRAW, true);
	}

TCArrow::TCArrow(char STYLE) : TCObject(0, 0, 0, 0, STYLE, CARROW) 
	{
		DYNAMIC_REDRAW(0, 0, DRAW, true);
	}

bool TCArrow::DYNAMIC_REDRAW(int new_X, int new_Y, DRAW_EVENT in_EVENT, bool is_UPDATE)
	{
		if(in_EVENT == REDRAW && new_X == X && new_Y == Y)
			return false;
		CPIXEL* BUF_L;
		if(in_EVENT == UNDRAW || in_EVENT ==  REDRAW)
			{
				BUF_L = hAPP->MAIN_SCR->FAKE_CBUFFER[X][Y];
				BUF_L->PREV_LAYER->NEXT_LAYER = NULL;
				hAPP->MAIN_SCR->FAKE_CBUFFER[X][Y] = BUF_L->PREV_LAYER;
				delete BUF_L;
			}
		if (in_EVENT == DRAW || in_EVENT == REDRAW)
			{
				X = new_X;
				Y = new_Y;
				BUF_L = new CPIXEL;
				BUF_L->NEXT_LAYER = NULL;
				BUF_L->hOWNER = this;
				BUF_L->OWNER_CODE = CODE;
				BUF_L->OWNER_TYPE = TYPE;
				BUF_L->PIX = (TCHAR)(BACKGROUND);
				BUF_L->PREV_LAYER = hAPP->MAIN_SCR->FAKE_CBUFFER[X][Y];
				hAPP->MAIN_SCR->FAKE_CBUFFER[X][Y]->NEXT_LAYER = BUF_L;
				hAPP->MAIN_SCR->FAKE_CBUFFER[X][Y] = BUF_L;
				if(is_UPDATE)
					hAPP->MAIN_SCR->REDRAW();
				return true;
			}
		if(is_UPDATE)
			hAPP->MAIN_SCR->REDRAW();
		return false;
	}

void TCArrow::REACT(int Mx, int My, WORD ACTION_TYPE)
	{
		switch(ACTION_TYPE)
			{
				case C_MMOVE:
					{
						if (RESIZED)
							{
								((TCWindow*)(RESIZED))->RESIZE(Mx-((TCWindow*)(RESIZED))->GET_X()+1, My-((TCWindow*)(RESIZED))->GET_Y()+1);
								DYNAMIC_REDRAW(X, Y, REDRAW, false);
							}
						else if(hAPP->MAIN_SCR->FAKE_CBUFFER[X][Y]->PREV_LAYER->OWNER_CODE != CODE && hAPP->MAIN_SCR->FAKE_CBUFFER[X][Y]->PREV_LAYER->hOWNER)
							((TCObject*)(hAPP->MAIN_SCR->FAKE_CBUFFER[X][Y]->PREV_LAYER->hOWNER))->REACT(X, Y, ACTION_TYPE);
						break;
					}
				case C_MUP:
					{
						if(CLICKED || RESIZED)
							{
								if(CLICKED)
									{
										CLICKED->REACT(X, Y, 2);
										CLICKED = NULL;
									}
								if(RESIZED)
									RESIZED = NULL;
							}
						else
							{
								if(STICKED)
									STICKED = NULL;
								if (hAPP->MAIN_SCR->FAKE_CBUFFER[X][Y]->PREV_LAYER->hOWNER)
									((TCObject*)(hAPP->MAIN_SCR->FAKE_CBUFFER[X][Y]->PREV_LAYER->hOWNER))->REACT(X, Y, ACTION_TYPE);
							}						
						break;
					}
				case C_KEY_INPUT:
					{
						if(SELECTED)
							SELECTED->REACT(Mx, My, 7);
						break;
					}
				case C_SELECT:
					{
						if(SELECTED_BEFORE)
							{
								if(SELECTED != SELECTED_BEFORE)
									{
										SELECTED_BEFORE->REACT(Mx, My, 9);
										SELECTED_BEFORE = NULL;
									}
							}
						if(SELECTED)
							{
								if(SELECTED != SELECTED_BEFORE)
									{
										SELECTED->REACT(Mx, My, 8);
										SELECTED_BEFORE = SELECTED;
									}
							}
						break;
					}
				default:
					{
						if (hAPP->MAIN_SCR->FAKE_CBUFFER[X][Y]->PREV_LAYER->hOWNER)
							((TCObject*)(hAPP->MAIN_SCR->FAKE_CBUFFER[X][Y]->PREV_LAYER->hOWNER))->REACT(X, Y, ACTION_TYPE);
						else
							SELECTED = NULL;
					}
			}
	}

//************************************************************************************************
//******** TCWindow
//************************************************************************************************

TCWindow::TCWindow(unsigned new_WIDTH, unsigned new_HEIGHT, int new_X, int new_Y, char in_BACKGROUND) : TCObject(WDH_HGH_RET(new_WIDTH), WDH_HGH_RET(new_HEIGHT), new_X, new_Y, in_BACKGROUND, CWINDOW) 
	{
		ACTION_ON_START_RESIZE = NULL;

		MAX_WIDTH = WIDTH;
		MAX_HEIGHT = HEIGHT;
		AVAILABLE_XMAX = WIDTH-1;
		AVAILABLE_XMIN = 1;
		AVAILABLE_YMAX = HEIGHT-1;
		AVAILABLE_YMIN = 1;
		CPIXEL* BUF;
		for(register int index = 1; index < (int)(WIDTH)-1; index++)
			{
				BUF = new CPIXEL;
				BUF->PIX = (TCHAR)(WIN_HORIZONTAL);
				BUF->hOWNER = NULL;
				BUF->OWNER_CODE = CODE;
				BUF->OWNER_TYPE = 0;
				BUF->NEXT_LAYER = NULL;
				BUF->PREV_LAYER = SPRITE[index][0];
				SPRITE[index][0]->NEXT_LAYER = BUF;
				SPRITE[index][0] = BUF;
				BUF = new CPIXEL;
				BUF->PIX = (TCHAR)(WIN_HORIZONTAL);
				BUF->hOWNER = NULL;
				BUF->OWNER_CODE = CODE;
				BUF->OWNER_TYPE = 0;
				BUF->NEXT_LAYER = NULL;
				BUF->PREV_LAYER = SPRITE[index][HEIGHT-1];
				SPRITE[index][HEIGHT-1]->NEXT_LAYER = BUF;
				SPRITE[index][HEIGHT-1] = BUF;
			}
		for (register int index = 1; index < (int)(HEIGHT)-1; index++)
			{
				BUF = new CPIXEL;
				BUF->PIX = (TCHAR)(WIN_VERTICAL);
				BUF->hOWNER = NULL;
				BUF->OWNER_CODE = CODE;
				BUF->OWNER_TYPE = 0;
				BUF->NEXT_LAYER = NULL;
				BUF->PREV_LAYER = SPRITE[0][index];
				SPRITE[0][index]->NEXT_LAYER = BUF;
				SPRITE[0][index] = BUF;
				BUF = new CPIXEL;
				BUF->PIX = (TCHAR)(WIN_VERTICAL);
				BUF->hOWNER = NULL;
				BUF->OWNER_CODE = CODE;
				BUF->OWNER_TYPE = 0;
				BUF->NEXT_LAYER = NULL;
				BUF->PREV_LAYER = SPRITE[WIDTH-1][index];
				SPRITE[WIDTH-1][index]->NEXT_LAYER = BUF;
				SPRITE[WIDTH-1][index] = BUF;
			}
		BUF = new CPIXEL;
		BUF->PIX = (TCHAR)(WIN_TOP_LEFT);
		BUF->hOWNER = NULL;
		BUF->OWNER_CODE = CODE;
		BUF->OWNER_TYPE = 0;
		BUF->NEXT_LAYER = NULL;
		BUF->PREV_LAYER = SPRITE[0][0];
		SPRITE[0][0]->NEXT_LAYER = BUF;
		SPRITE[0][0] = BUF;

		BUF = new CPIXEL;
		BUF->PIX = (TCHAR)(WIN_TOP_RIGHT);
		BUF->hOWNER = NULL;
		BUF->OWNER_CODE = CODE;
		BUF->OWNER_TYPE = 0;
		BUF->NEXT_LAYER = NULL;
		BUF->PREV_LAYER = SPRITE[WIDTH-1][0];
		SPRITE[WIDTH-1][0]->NEXT_LAYER = BUF;
		SPRITE[WIDTH-1][0] = BUF;

		BUF = new CPIXEL;
		BUF->PIX = (TCHAR)(WIN_BOTTOM_LEFT);
		BUF->hOWNER = NULL;
		BUF->OWNER_CODE = CODE;
		BUF->OWNER_TYPE = 0;
		BUF->NEXT_LAYER = NULL;
		BUF->PREV_LAYER = SPRITE[0][HEIGHT-1];
		SPRITE[0][HEIGHT-1]->NEXT_LAYER = BUF;
		SPRITE[0][HEIGHT-1] = BUF;

		BUF = new CPIXEL;
		BUF->PIX = WIN_BOTTOM_RIGHT;
		BUF->hOWNER = NULL;
		BUF->OWNER_CODE = CODE;
		BUF->OWNER_TYPE = 0;
		BUF->NEXT_LAYER = NULL;
		BUF->PREV_LAYER = SPRITE[WIDTH-1][HEIGHT-1];
		SPRITE[WIDTH-1][HEIGHT-1]->NEXT_LAYER = BUF;
		SPRITE[WIDTH-1][HEIGHT-1] = BUF;

		if(hAPP->IN_PROCESS())
			DYNAMIC_REDRAW(new_X, new_Y, DRAW, true);
		else
			DYNAMIC_REDRAW(new_X, new_Y, DRAW, false);
	}

void TCWindow::RESIZE(int new_WIDTH, int new_HEIGHT)
	{
		if((new_WIDTH > MAX_WIDTH && new_HEIGHT > MAX_HEIGHT) || (new_WIDTH < 2 && new_HEIGHT < 2))
			{
				if(ACTION_ON_START_RESIZE)
					ACTION_ON_START_RESIZE(this, ((TCArrow*)(hAPP->ARROW))->GET_X(), ((TCArrow*)(hAPP->ARROW))->GET_Y(), WIDTH, HEIGHT);
				return;
			}
		if(ACTION_ON_START_RESIZE)
			ACTION_ON_START_RESIZE(this, ((TCArrow*)(hAPP->ARROW))->GET_X(), ((TCArrow*)(hAPP->ARROW))->GET_Y(), new_WIDTH, new_HEIGHT);
		if(new_WIDTH > MAX_WIDTH)
			new_WIDTH = MAX_WIDTH;
		if(new_HEIGHT > MAX_HEIGHT)
			new_HEIGHT = MAX_HEIGHT;
		if(new_WIDTH < 2)
			new_WIDTH = 2;
		if(new_HEIGHT < 2)
			new_HEIGHT = 2;
		CPIXEL* BUF;
		DYNAMIC_REDRAW(X, Y, UNDRAW, false);
		for (register int index = 1; index < (int)(WIDTH)-1; index++)
			{
				BUF = SPRITE[index][0];
				SPRITE[index][0] = BUF->PREV_LAYER;
				SPRITE[index][0]->NEXT_LAYER = NULL;
				delete BUF;
				BUF = SPRITE[index][HEIGHT-1];
				SPRITE[index][HEIGHT-1] = BUF->PREV_LAYER;
				SPRITE[index][HEIGHT-1]->NEXT_LAYER = NULL;
				delete BUF;
			}
		for (register int index = 1; index < (int)(HEIGHT)-1; index++)
			{
				BUF = SPRITE[0][index];
				SPRITE[0][index] = BUF->PREV_LAYER;
				SPRITE[0][index]->NEXT_LAYER = NULL;
				delete BUF;
				BUF = SPRITE[WIDTH-1][index];
				SPRITE[WIDTH-1][index] = BUF->PREV_LAYER;
				SPRITE[WIDTH-1][index]->NEXT_LAYER = NULL;
				delete BUF;
			}
		BUF = SPRITE[0][0];
		SPRITE[0][0] = BUF->PREV_LAYER;
		SPRITE[0][0]->NEXT_LAYER = NULL;
		delete BUF;

		BUF = SPRITE[0][HEIGHT-1];
		SPRITE[0][HEIGHT-1] = BUF->PREV_LAYER;
		SPRITE[0][HEIGHT-1]->NEXT_LAYER = NULL;
		delete BUF;

		BUF = SPRITE[WIDTH-1][0];
		SPRITE[WIDTH-1][0] = BUF->PREV_LAYER;
		SPRITE[WIDTH-1][0]->NEXT_LAYER = NULL;
		delete BUF;

		BUF = SPRITE[WIDTH-1][HEIGHT-1];
		SPRITE[WIDTH-1][HEIGHT-1] = BUF->PREV_LAYER;
		SPRITE[WIDTH-1][HEIGHT-1]->NEXT_LAYER = NULL;
		delete BUF;

		WIDTH = WDH_HGH_RET(new_WIDTH);
		HEIGHT = WDH_HGH_RET(new_HEIGHT);

		for(register int index = 1; index < (int)(WIDTH)-1; index++)
			{
				BUF = new CPIXEL;
				BUF->PIX = (TCHAR)(WIN_HORIZONTAL);
				BUF->hOWNER = NULL;
				BUF->OWNER_CODE = CODE;
				BUF->OWNER_TYPE = 0;
				BUF->NEXT_LAYER = NULL;
				BUF->PREV_LAYER = SPRITE[index][0];
				SPRITE[index][0]->NEXT_LAYER = BUF;
				SPRITE[index][0] = BUF;
				BUF = new CPIXEL;
				BUF->PIX = (TCHAR)(WIN_HORIZONTAL);
				BUF->hOWNER = NULL;
				BUF->OWNER_CODE = CODE;
				BUF->OWNER_TYPE = 0;
				BUF->NEXT_LAYER = NULL;
				BUF->PREV_LAYER = SPRITE[index][HEIGHT-1];
				SPRITE[index][HEIGHT-1]->NEXT_LAYER = BUF;
				SPRITE[index][HEIGHT-1] = BUF;
			}
		for (register int index = 1; index < (int)(HEIGHT)-1; index++)
			{
				BUF = new CPIXEL;
				BUF->PIX = (TCHAR)(WIN_VERTICAL);
				BUF->hOWNER = NULL;
				BUF->OWNER_CODE = CODE;
				BUF->OWNER_TYPE = 0;
				BUF->NEXT_LAYER = NULL;
				BUF->PREV_LAYER = SPRITE[0][index];
				SPRITE[0][index]->NEXT_LAYER = BUF;
				SPRITE[0][index] = BUF;
				BUF = new CPIXEL;
				BUF->PIX = (TCHAR)(WIN_VERTICAL);
				BUF->hOWNER = NULL;
				BUF->OWNER_CODE = CODE;
				BUF->OWNER_TYPE = 0;
				BUF->NEXT_LAYER = NULL;
				BUF->PREV_LAYER = SPRITE[WIDTH-1][index];
				SPRITE[WIDTH-1][index]->NEXT_LAYER = BUF;
				SPRITE[WIDTH-1][index] = BUF;
			}
		BUF = new CPIXEL;
		BUF->PIX = (TCHAR)(WIN_TOP_LEFT);
		BUF->hOWNER = NULL;
		BUF->OWNER_CODE = CODE;
		BUF->OWNER_TYPE = 0;
		BUF->NEXT_LAYER = NULL;
		BUF->PREV_LAYER = SPRITE[0][0];
		SPRITE[0][0]->NEXT_LAYER = BUF;
		SPRITE[0][0] = BUF;

		BUF = new CPIXEL;
		BUF->PIX = (TCHAR)(WIN_TOP_RIGHT);
		BUF->hOWNER = NULL;
		BUF->OWNER_CODE = CODE;
		BUF->OWNER_TYPE = 0;
		BUF->NEXT_LAYER = NULL;
		BUF->PREV_LAYER = SPRITE[WIDTH-1][0];
		SPRITE[WIDTH-1][0]->NEXT_LAYER = BUF;
		SPRITE[WIDTH-1][0] = BUF;

		BUF = new CPIXEL;
		BUF->PIX = (TCHAR)(WIN_BOTTOM_LEFT);
		BUF->hOWNER = NULL;
		BUF->OWNER_CODE = CODE;
		BUF->OWNER_TYPE = 0;
		BUF->NEXT_LAYER = NULL;
		BUF->PREV_LAYER = SPRITE[0][HEIGHT-1];
		SPRITE[0][HEIGHT-1]->NEXT_LAYER = BUF;
		SPRITE[0][HEIGHT-1] = BUF;

		BUF = new CPIXEL;
		BUF->PIX = WIN_BOTTOM_RIGHT;
		BUF->hOWNER = NULL;
		BUF->OWNER_CODE = CODE;
		BUF->OWNER_TYPE = 0;
		BUF->NEXT_LAYER = NULL;
		BUF->PREV_LAYER = SPRITE[WIDTH-1][HEIGHT-1];
		SPRITE[WIDTH-1][HEIGHT-1]->NEXT_LAYER = BUF;
		SPRITE[WIDTH-1][HEIGHT-1] = BUF;

		AVAILABLE_XMAX = WIDTH-1;
		AVAILABLE_YMAX = HEIGHT-1;
		
		DYNAMIC_REDRAW(X, Y, DRAW, false);
	}

void TCWindow::REACT(int Mx, int My, WORD ACTION_TYPE)
	{
		if(ACTION_TYPE < (WORD)C_KEY_INPUT)
			{
				if(SPRITE[Mx-X][My-Y]->hOWNER)
					{
						((TCComponent*)(SPRITE[Mx-X][My-Y]->hOWNER))->REACT(Mx-X, My-Y, ACTION_TYPE);
						return;
					}
			}
		switch (ACTION_TYPE)
			{
				case C_MMOVE: 
					{
						if(ACTION_ON_MOUSE_MOVE)
							ACTION_ON_MOUSE_MOVE(this, Mx, My);
						return;
					}
				case C_MDOWNL:
					{
						if(ACTION_ON_MOUSE_DOWN)	
							ACTION_ON_MOUSE_DOWN(this, Mx, My);
						if(ENABLED)
							{
								if (Mx == X+WIDTH-1 && My == Y+HEIGHT-1)
									((TCArrow*)(hAPP->ARROW))->RESIZED = this;
								else if(Mx == X+WIDTH-1 && My == Y)
									{
										((TCArrow*)(hAPP->ARROW))->SELECTED = NULL;
										DYNAMIC_REDRAW(X, Y, UNDRAW, true);
									}
								else
									{
										((TCArrow*)(hAPP->ARROW))->SELECTED = this;
										((TCArrow*)(hAPP->ARROW))->STICKED = this;
									}
							}
						return;
					}
				case C_MDOWNR:
					{
						if(ENABLED && MENU)
							{
								((TCObject*)(MENU))->DYNAMIC_REDRAW(Mx, My, REDRAW, true);
								((TCArrow*)(hAPP->ARROW))->SELECTED = MENU;
								((TCArrow*)(hAPP->ARROW))->REACT(Mx, My, 8);
							}
						if(ACTION_ON_MOUSE_DOWN_R)
							ACTION_ON_MOUSE_DOWN_R(this, Mx, My);
						return;
					}
				case C_MUP:
					{
						if(ACTION_ON_MOUSE_UP) 
							ACTION_ON_MOUSE_UP(this, Mx, My, ((TCArrow*)(hAPP->ARROW))->LAST_PRESSED);
						return;
					}
				case C_MDBCLICK:
					{
						if(ACTION_ON_MOUSE_DBCLICK)
							ACTION_ON_MOUSE_DBCLICK(this, Mx, My, ((TCArrow*)(hAPP->ARROW))->LAST_PRESSED);
						return;
					}
				case C_MSCROLL_UP:
					{
						if(ACTION_ON_SCROLL_UP)
							ACTION_ON_SCROLL_UP(this, Mx, My);
						return;
					}
				case C_MSCROLL_DOWN:
					{
						if(ACTION_ON_SCROLL_DOWN)
							ACTION_ON_SCROLL_DOWN(this, Mx, My);
						return;
					}
				case C_SELECT:
					{
						if(ENABLED)
							DYNAMIC_REDRAW(X, Y, REDRAW, true);
						if(ACTION_ON_SELECT)
							ACTION_ON_SELECT(this, Mx, My);
						return;
					}
				case C_UNSELECT:
					{
						if(ACTION_ON_UNSELECT)
							ACTION_ON_UNSELECT(this, Mx, My);
						return;
					}
			}
	}

//************************************************************************************************
//******** TCSubMenu
//************************************************************************************************

TCSubMenu::TCSubMenu(unsigned new_WIDTH, unsigned new_HEIGHT, char in_BACKGROUND, char in_SHADOW, TCObject* new_PARENT) : TCObject(WDH_HGH_RET(new_WIDTH), WDH_HGH_RET(new_HEIGHT), 0, 0, in_BACKGROUND, CSUBMENU)
	{
		for(register int index = 1; index < (int)(HEIGHT); index++)
			SPRITE[WIDTH-1][index]->PIX = in_SHADOW;
		for(register int index = 1; index < (int)(WIDTH-1); index++)
			SPRITE[index][HEIGHT-1]->PIX = in_SHADOW;
		SPRITE[WIDTH-1][0]->PIX = NULL;
		SPRITE[0][HEIGHT-1]->PIX = NULL;
		PARENT = new_PARENT;
		FIRST  = NULL;
		SELECTED = NULL;
		COUNT  = 0;
		new_PARENT->MENU = this;
	}

TCSubMenu::~TCSubMenu()
	{
		CSUB_MENU_STRING* BUF;
		while(FIRST)
			{
				BUF = FIRST;
				FIRST = FIRST->NEXT;
				delete BUF;
			}
	}

void TCSubMenu::POP()
	{
		if(FIRST)
			{
				if(FIRST->NEXT)
					{
						FIRST = FIRST->NEXT;
						delete FIRST->PREV;
						FIRST->PREV = NULL;
					}
				else
					{
						delete FIRST;
						FIRST = NULL;
					}
				COUNT--;
			}
		UPDATE(false);
	}

void TCSubMenu::PUSH(const char* STR, char UNSEL, char SEL, unsigned OFFSET, void (*ACTION) (void))
	{
		CSUB_MENU_STRING* new_STR = new CSUB_MENU_STRING;
		new_STR->ACTION_ON_CLICK = ACTION;
		new_STR->STRING = STR;
		new_STR->SELECTED_STYLE = SEL;
		new_STR->UNSELECTED_STYLE = UNSEL;
		new_STR->OFFSET = OFFSET;
		new_STR->NEXT = FIRST;
		if(FIRST)
			FIRST->PREV = new_STR;
		FIRST = new_STR;
		COUNT++;
		UPDATE(false);
	}

void TCSubMenu::PUSH(const char* STR, char UNSEL, char SEL, unsigned OFFSET)
	{
		CSUB_MENU_STRING* new_STR = new CSUB_MENU_STRING;
		new_STR->ACTION_ON_CLICK = NULL;
		new_STR->STRING = STR;
		new_STR->SELECTED_STYLE = SEL;
		new_STR->UNSELECTED_STYLE = UNSEL;
		new_STR->OFFSET = OFFSET;
		new_STR->NEXT = FIRST;
		if(FIRST)
			FIRST->PREV = new_STR;
		FIRST = new_STR;
		COUNT++;
		UPDATE(false);
	}

void TCSubMenu::PUSH(const char* STR, char UNSEL, char SEL)
	{
		CSUB_MENU_STRING* new_STR = new CSUB_MENU_STRING;
		new_STR->ACTION_ON_CLICK = NULL;
		new_STR->STRING = STR;
		new_STR->SELECTED_STYLE = SEL;
		new_STR->UNSELECTED_STYLE = UNSEL;
		new_STR->OFFSET = 0;
		new_STR->NEXT = FIRST;
		if(FIRST)
			FIRST->PREV = new_STR;
		FIRST = new_STR;
		COUNT++;
		UPDATE(false);
	}

void TCSubMenu::PUSH(char UNSEL, char SEL)
	{
		CSUB_MENU_STRING* new_STR = new CSUB_MENU_STRING;
		new_STR->ACTION_ON_CLICK = NULL;
		new_STR->STRING = "";
		new_STR->SELECTED_STYLE = SEL;
		new_STR->UNSELECTED_STYLE = UNSEL;
		new_STR->OFFSET = 0;
		new_STR->NEXT = FIRST;
		if(FIRST)
			FIRST->PREV = new_STR;
		FIRST = new_STR;
		COUNT++;
		UPDATE(false);
	}

CSUB_MENU_STRING* TCSubMenu::GET_BY_INDEX(register int index)
	{
		CSUB_MENU_STRING* BUF = FIRST;
		while(BUF && index-- > 0)
				BUF = BUF->NEXT;
		return BUF;
	}

CSUB_MENU_STRING_INFO* TCSubMenu::GET_INFO_BY_INDEX (register int index)
	{
		CSUB_MENU_STRING_INFO* RET = new CSUB_MENU_STRING_INFO;
		CSUB_MENU_STRING* BUF = GET_BY_INDEX(index);
		RET->ACTION_ON_CLICK = &BUF->ACTION_ON_CLICK;
		RET->OFFSET = &BUF->OFFSET;
		RET->SELECTED_STYLE = &BUF->SELECTED_STYLE;
		RET->STRING = &BUF->STRING;
		RET->UNSELECTED_STYLE = &BUF->UNSELECTED_STYLE;
		return RET;
	}

CSUB_MENU_STRING_INFO* TCSubMenu::GET_SELECTED_INFO()
	{
		CSUB_MENU_STRING_INFO* RET = new CSUB_MENU_STRING_INFO;
		RET->ACTION_ON_CLICK = &SELECTED->ACTION_ON_CLICK;
		RET->OFFSET = &SELECTED->OFFSET;
		RET->SELECTED_STYLE = &SELECTED->SELECTED_STYLE;
		RET->STRING = &SELECTED->STRING;
		RET->UNSELECTED_STYLE = &SELECTED->UNSELECTED_STYLE;
		return RET;
	}

int TCSubMenu::GET_COUNT()
	{
		return COUNT;
	}

void TCSubMenu::UPDATE(bool SHOW)
	{
		CSUB_MENU_STRING* BUF = FIRST;
		register int xindex, yindex = 0;
		while(BUF && yindex < (int)(HEIGHT-1))
			{
				xindex = 0;
				if(BUF->SELECTED)
					{
						while(xindex < (int)(BUF->OFFSET) && xindex < (int)(WIDTH-1))
							{
								SPRITE[xindex][yindex]->PIX = BUF->SELECTED_STYLE;
								xindex++;
							}
					}
				else
					{
						while(xindex < (int)(BUF->OFFSET) && xindex < (int)(WIDTH-1))
							{
								SPRITE[xindex][yindex]->PIX = BUF->UNSELECTED_STYLE;
								xindex++;
							}
					}
				while(xindex-BUF->OFFSET < (int)(strlen(BUF->STRING)) && xindex < (int)(WIDTH-1))
					{
						SPRITE[xindex][yindex]->PIX = BUF->STRING[xindex-BUF->OFFSET];
						xindex++;
					}
				if(BUF->SELECTED)
					{
						while(xindex < (int)(WIDTH-1))
							{
								SPRITE[xindex][yindex]->PIX = BUF->SELECTED_STYLE;
								xindex++;
							}
					}
				else
					{
						while(xindex < (int)(WIDTH-1))
							{
								SPRITE[xindex][yindex]->PIX = BUF->UNSELECTED_STYLE;
								xindex++;
							}
					}
				yindex++;
				BUF = BUF->NEXT;
			}
		if(yindex < (int)(HEIGHT-1))
			{
				while(yindex < (int)(HEIGHT-1))
					{
						for(int xindex = 0; xindex < (int)(WIDTH-1); xindex++)
							SPRITE[xindex][yindex]->PIX = BACKGROUND;
						yindex++;
					}
			}
		if(SHOW)
			{
				if(hAPP->IN_PROCESS())
					STATIC_REDRAW(true);
				else
					STATIC_REDRAW(false);
			}
	}

void TCSubMenu::REACT(int Mx, int My, WORD ACTION_TYPE)
	{
		switch (ACTION_TYPE)
			{
				case C_MMOVE: 
					{
						if(ENABLED)
							CHECK(Mx, My, 0);
						if(ACTION_ON_MOUSE_MOVE)
							ACTION_ON_MOUSE_MOVE(this, Mx, My);
						return;
					}
				case C_MDOWNL:
					{
						if(ENABLED)
							CHECK(Mx, My, 1);
						if(ACTION_ON_MOUSE_DOWN)	
							ACTION_ON_MOUSE_DOWN(this, Mx, My);
						return;
					}
				case C_MDOWNR:
					{
						if(ACTION_ON_MOUSE_DOWN_R)
							ACTION_ON_MOUSE_DOWN_R(this, Mx, My);
						return;
					}
				case C_MUP:
					{
						if(ACTION_ON_MOUSE_UP) 
							ACTION_ON_MOUSE_UP(this, Mx, My, ((TCArrow*)(hAPP->ARROW))->LAST_PRESSED);
						return;
					}
				case C_MDBCLICK:
					{
						if(ACTION_ON_MOUSE_DBCLICK)
							ACTION_ON_MOUSE_DBCLICK(this, Mx, My, ((TCArrow*)(hAPP->ARROW))->LAST_PRESSED);
						return;
					}
				case C_MSCROLL_UP:
					{
						if(ACTION_ON_SCROLL_UP)
							ACTION_ON_SCROLL_UP(this, Mx, My);
						return;
					}
				case C_MSCROLL_DOWN:
					{
						if(ACTION_ON_SCROLL_DOWN)
							ACTION_ON_SCROLL_DOWN(this, Mx, My);
						return;
					}
				case C_SELECT:
					{
						DYNAMIC_REDRAW(X, Y, REDRAW, true);
						if(ACTION_ON_SELECT)
							ACTION_ON_SELECT(this, Mx, My);
						return;
					}
				case C_UNSELECT:
					{
						CHECK(Mx, My, 9);
						if(ACTION_ON_UNSELECT)
							ACTION_ON_UNSELECT(this, Mx, My);
						return;
					}
			}
	}

void TCSubMenu::CHECK(int Mx, int My, WORD ACTION_TYPE)
	{
		switch(ACTION_TYPE)
			{
				case 0:
					{
						CSUB_MENU_STRING* BUF = GET_BY_INDEX(My-Y);
						if(BUF)
							{
								if(SELECTED)
									SELECTED->SELECTED = false; 
								BUF->SELECTED = true;
								SELECTED = BUF;
							}
						UPDATE(true);
						return;
					}
				case 1:
					{
						DYNAMIC_REDRAW(X, Y, UNDRAW, true);
						CSUB_MENU_STRING* _BUF = GET_BY_INDEX(My-Y);
						if(SELECTED)
							SELECTED->SELECTED = false;
						SELECTED = NULL; 
						if(_BUF)
							{
								if(_BUF->ACTION_ON_CLICK)
									_BUF->ACTION_ON_CLICK();
							}
						return;
					}
				case 9:
					{
						DYNAMIC_REDRAW(X, Y, UNDRAW, true);
						if(SELECTED)
							SELECTED->SELECTED = false;
						SELECTED = NULL; 
					}
			}
	}

//************************************************************************************************
//******** TCMainMenu
//************************************************************************************************

TCMainMenu::TCMainMenu(CMAIN_MENU_POSITION in_ALIGN, int in_WIDTH, char in_BACKGROUND) : TCObject(0, 0, 0, 0, in_BACKGROUND, CMAINMENU)
	{
		ALIGN		 = in_ALIGN;
		COUNT		 = 0;
		FIRST		 = NULL;
		LAST		 = NULL;
		FIRST_DRAWED = NULL;
		MENU_WIDTH	 = in_WIDTH;
		switch(ALIGN)
			{
				case C_TOP:
					{
						X = 0;
						Y = 0;
						WIDTH = SCREEN_WIDTH;
						HEIGHT = MENU_WIDTH;
						break;
					}
				case C_BOTTOM:
					{
						X = 0;
						Y = SCREEN_HEIGHT-MENU_WIDTH;
						WIDTH = SCREEN_WIDTH;
						HEIGHT = MENU_WIDTH;
						break;
					}
				case C_LEFT:
					{
						X = 0;
						Y = 0;
						WIDTH = MENU_WIDTH;
						HEIGHT = SCREEN_HEIGHT;
						break;
					}
				default:
					{
						X = SCREEN_WIDTH-MENU_WIDTH;
						Y = 0;
						WIDTH = MENU_WIDTH;
						HEIGHT = SCREEN_HEIGHT;
					}
			}
		SPRITE = new CPIXEL**[WIDTH];
		for(register int xindex = 0; xindex < WIDTH; xindex++)
			{
				SPRITE[xindex] = new CPIXEL*[HEIGHT];
				for(register int yindex = 0; yindex < HEIGHT; yindex++)
					{
						SPRITE[xindex][yindex]			   = new CPIXEL;
						SPRITE[xindex][yindex]->hOWNER	   = NULL;
						SPRITE[xindex][yindex]->PIX		   = BACKGROUND;
						SPRITE[xindex][yindex]->NEXT_LAYER = NULL;
						SPRITE[xindex][yindex]->PREV_LAYER = NULL;
						SPRITE[xindex][yindex]->OWNER_CODE = 0;
						SPRITE[xindex][yindex]->OWNER_TYPE = 0;
					}
			}
		if(hAPP->IN_PROCESS())
			DYNAMIC_REDRAW(X, Y, DRAW, true);
		else
			DYNAMIC_REDRAW(X, Y, DRAW, false);
	}

void TCMainMenu::RE_INDEX(CMAIN_MENU_SECTION* BUF, bool PLUS)
	{
		if(PLUS)
			{
				while(BUF)
					{
						BUF->INDEX++;
						BUF = BUF->NEXT;
					}
			}
		else
			{
				while(BUF)
					{
						BUF->INDEX--;
						BUF = BUF->NEXT;
					}
			}
	}

void TCMainMenu::MOVE_DOWN()
	{
		if(FIRST_DRAWED->NEXT)
			{
				if(LAST->POS != -1)
					{
						switch(ALIGN)
							{
								case C_LEFT:
									{
										for(register int index = 0; index < strlen(LAST->SECTION_NAME) && LAST->POS+index < SCREEN_HEIGHT; index++)
											{
												SPRITE[MENU_WIDTH-1][LAST->POS+index]->PIX		  = BACKGROUND;
												SPRITE[MENU_WIDTH-1][LAST->POS+index]->hOWNER	  = NULL;
												SPRITE[MENU_WIDTH-1][LAST->POS+index]->OWNER_CODE = 0;
												SPRITE[MENU_WIDTH-1][LAST->POS+index]->OWNER_TYPE = 0;
												for(int windex = MENU_WIDTH-2; windex >= 0; windex--)
													SPRITE[windex][LAST->POS+index]->PIX	  = BACKGROUND;
											}
										break;
									}
								case C_RIGHT:
									{
										for(register int index = 0; index < strlen(LAST->SECTION_NAME) && LAST->POS+index < SCREEN_HEIGHT; index++)
											{
												SPRITE[0][LAST->POS+index]->PIX		   = BACKGROUND;
												SPRITE[0][LAST->POS+index]->hOWNER	   = NULL;
												SPRITE[0][LAST->POS+index]->OWNER_CODE = 0;
												SPRITE[0][LAST->POS+index]->OWNER_TYPE = 0;
												for(int windex = 1; windex < MENU_WIDTH; windex++)
													SPRITE[windex][LAST->POS+index]->PIX	   = BACKGROUND;
											}
										break;
									}
								case C_TOP:
									{
										for(register int index = 0; index < strlen(LAST->SECTION_NAME) && LAST->POS+index < SCREEN_WIDTH; index++)
											{											
												SPRITE[LAST->POS+index][MENU_WIDTH-1]->PIX		  = BACKGROUND;
												SPRITE[LAST->POS+index][MENU_WIDTH-1]->hOWNER	  = NULL;
												SPRITE[LAST->POS+index][MENU_WIDTH-1]->OWNER_CODE = 0;
												SPRITE[LAST->POS+index][MENU_WIDTH-1]->OWNER_TYPE = 0;
												for(int windex = MENU_WIDTH-2; windex >= 0; windex--)
													SPRITE[LAST->POS+index][windex]->PIX	  = BACKGROUND;
											}
										break;
									}
								default:
									{
										for(register int index = 0; index < strlen(LAST->SECTION_NAME) && LAST->POS+index < SCREEN_WIDTH; index++)
											{
												SPRITE[LAST->POS+index][0]->PIX		   = BACKGROUND;
												SPRITE[LAST->POS+index][0]->hOWNER	   = NULL;
												SPRITE[LAST->POS+index][0]->OWNER_CODE = 0;
												SPRITE[LAST->POS+index][0]->OWNER_TYPE = 0;
												for(int windex = 1; windex < MENU_WIDTH; windex++)
													SPRITE[LAST->POS+index][windex]->PIX = BACKGROUND;
											}
									}
							}
					}
				FIRST_DRAWED = FIRST_DRAWED->NEXT;
				UPDATE(true);
			}
	}

void TCMainMenu::MOVE_UP()
	{
		if(FIRST_DRAWED->PREV)
			{
				FIRST_DRAWED = FIRST_DRAWED->PREV;
				UPDATE(true);
			}
	}

void TCMainMenu::ERASE(int index)
	{
		CMAIN_MENU_SECTION* BUF			 = GET_SECTION_BY_INDEX(index);
		if(BUF)
			{
				RE_INDEX(BUF->NEXT, false);
				if(BUF->NEXT)
					BUF->NEXT->PREV		 = BUF->PREV;
				if(BUF->PREV)
					BUF->PREV->NEXT		 = BUF->NEXT;
				if(BUF == FIRST_DRAWED)
					{
						if(BUF->NEXT)
							FIRST_DRAWED = BUF->NEXT;
						else
							FIRST_DRAWED = BUF->PREV;
					}
				delete BUF;
				COUNT--;
			}
		UPDATE(true);
	}

void TCMainMenu::INSERT(int index, TCSubMenu* in_MENU, char* NAME, char in_BACKGROUND, char in_SEL_BACKGROUND)
	{
		CMAIN_MENU_SECTION* BUF = GET_SECTION_BY_INDEX(index);
		if(BUF)
			{
				CMAIN_MENU_SECTION* BUF_I = new CMAIN_MENU_SECTION;
				BUF_I->NEXT				  = BUF->NEXT;
				BUF_I->PREV				  = BUF;
				BUF_I->BACKGROUND		  = in_BACKGROUND;
				BUF_I->INDEX			  = BUF_I->PREV->INDEX+1;
				BUF_I->SECTION_MENU		  = in_MENU;
				BUF_I->SECTION_NAME		  = new char[strlen(NAME)];
				strcpy(BUF_I->SECTION_NAME, NAME);
				RE_INDEX(BUF_I->NEXT, true);
				if(BUF->NEXT)
					BUF->NEXT->PREV		  = BUF_I;
				BUF->NEXT				  = BUF_I;
				COUNT++;
			}
		UPDATE(true);
	}

void TCMainMenu::ADD_TO_END(TCSubMenu* in_MENU, char* NAME, char in_BACKGROUND)
	{
		if(LAST)
			{
				LAST->NEXT				 = new CMAIN_MENU_SECTION;
				LAST->NEXT->NEXT		 = NULL;
				LAST->NEXT->PREV		 = LAST;
				LAST->NEXT->BACKGROUND   = in_BACKGROUND;
				LAST->NEXT->INDEX = LAST->INDEX+1; 
				LAST->NEXT->SECTION_MENU = in_MENU;
				LAST->NEXT->SECTION_NAME = new char[strlen(NAME)];
				strcpy(LAST->NEXT->SECTION_NAME, NAME);
				LAST = LAST->NEXT; 
			}
		else
			{
				LAST			   = new CMAIN_MENU_SECTION;
				LAST->NEXT		   = NULL;
				LAST->PREV		   = NULL;
				LAST->BACKGROUND   = in_BACKGROUND;
				LAST->INDEX		   = 0;
				LAST->SECTION_MENU = in_MENU;
				LAST->SECTION_NAME = new char[strlen(NAME)];
				strcpy(LAST->SECTION_NAME, NAME);
				FIRST			   = LAST;
				FIRST_DRAWED	   = LAST;
			}
		UPDATE(true);
	}

void TCMainMenu::ADD_TO_START(TCSubMenu* in_MENU, char* NAME, char in_BACKGROUND)
	{
		if(FIRST)
			{
				FIRST->PREV				  = new CMAIN_MENU_SECTION;
				FIRST->PREV->NEXT		  = FIRST;
				FIRST->PREV->PREV		  = NULL;
				FIRST->PREV->BACKGROUND	  = in_BACKGROUND;
				FIRST->PREV->INDEX		  = 0;
				RE_INDEX(FIRST->PREV->NEXT, true);
				FIRST->PREV->SECTION_MENU = in_MENU;
				FIRST->PREV->SECTION_NAME = new char[strlen(NAME)];
				strcpy(FIRST->PREV->SECTION_NAME, NAME);
				FIRST					  = FIRST->PREV;
			}
		else
			{
				FIRST				= new CMAIN_MENU_SECTION;
				FIRST->NEXT			= NULL;
				FIRST->PREV			= NULL;
				FIRST->BACKGROUND	= in_BACKGROUND;
				FIRST->INDEX		= 0;
				FIRST->SECTION_MENU = in_MENU;
				FIRST->SECTION_NAME = new char[strlen(NAME)];
				strcpy(FIRST->SECTION_NAME, NAME);
				LAST				= FIRST;
				FIRST_DRAWED		= FIRST;
			}
		COUNT++;
		UPDATE(true);
	}

TCSubMenu* TCMainMenu::GET_BY_INDEX(int index)
	{
		return (GET_SECTION_BY_INDEX(index))->SECTION_MENU;
	}

CMAIN_MENU_SECTION* TCMainMenu::GET_SECTION_BY_INDEX(register int index)
	{
		CMAIN_MENU_SECTION* BUF = FIRST;
		while(index-- > 0 && BUF)
			BUF = BUF->NEXT;
		return BUF;
	}

void TCMainMenu::UPDATE(bool SHOW)
	{
		CMAIN_MENU_SECTION* BUF = FIRST_DRAWED;
		register int sindex, index = 0;
		switch(ALIGN)
			{
				case C_RIGHT:
					{
						while(BUF)
							{
								sindex = index;
								if(index < SCREEN_HEIGHT)
									BUF->POS = index;
								else
									BUF->POS = -1;
								while(index-sindex < strlen(BUF->SECTION_NAME) && index < SCREEN_HEIGHT)
									{
										if(BUF->SECTION_NAME[index-sindex] != 32)
											SPRITE[0][index]->PIX = BUF->SECTION_NAME[index-sindex];
										else
											SPRITE[0][index]->PIX = BACKGROUND;
										for(int windex = 1; windex < MENU_WIDTH; windex++)
											SPRITE[windex][index]->PIX = BUF->BACKGROUND;
										SPRITE[0][index]->hOWNER = BUF->SECTION_MENU;
										SPRITE[0][index]->OWNER_CODE = BUF->INDEX;
										SPRITE[0][index]->OWNER_TYPE = 8;
										index++;
									}
								if(index < SCREEN_HEIGHT)
									{
										SPRITE[0][index]->PIX = BACKGROUND;
										SPRITE[0][index]->hOWNER = NULL;
										SPRITE[0][index]->OWNER_CODE = 0;
										SPRITE[0][index]->OWNER_TYPE = 0;
										for(int windex = 1; windex < MENU_WIDTH; windex++)
											SPRITE[windex][index]->PIX = BACKGROUND;
									}
								index++;
								BUF = BUF->NEXT;
							}
						break;
					}
				case C_LEFT:
					{
						while(BUF)
							{
								sindex = index;
								if(index < SCREEN_HEIGHT)
									BUF->POS = index;
								else
									BUF->POS = -1;
								while(index-sindex < strlen(BUF->SECTION_NAME) && index < SCREEN_HEIGHT)
									{
										if(BUF->SECTION_NAME[index-sindex] != BACKGROUND)
											SPRITE[WIDTH-1][index]->PIX = BUF->SECTION_NAME[index-sindex];
										else
											SPRITE[WIDTH-1][index]->PIX = BACKGROUND;
										for(int windex = WIDTH-2; windex >= 0; windex--)
											SPRITE[windex][index]->PIX = BUF->BACKGROUND;
										SPRITE[WIDTH-1][index]->hOWNER = BUF->SECTION_MENU;
										SPRITE[WIDTH-1][index]->OWNER_CODE = BUF->INDEX;
										SPRITE[WIDTH-1][index]->OWNER_TYPE = 8;
										index++;
									}
								if(index < SCREEN_HEIGHT)
									{
										SPRITE[WIDTH-1][index]->PIX = BACKGROUND;
										SPRITE[WIDTH-1][index]->hOWNER = NULL;
										SPRITE[WIDTH-1][index]->OWNER_CODE = 0;
										SPRITE[WIDTH-1][index]->OWNER_TYPE = 0;
										for(int windex = WIDTH-2; windex >= 0; windex--)
											SPRITE[windex][index]->PIX = BACKGROUND;	
									}
								index++;
								BUF = BUF->NEXT;
							}
						break;
					}
				case C_BOTTOM:
					{
						while(BUF)
							{
								sindex = index;
								if(index < SCREEN_WIDTH)
									BUF->POS = index;
								else
									BUF->POS = -1;
								while(index-sindex < strlen(BUF->SECTION_NAME) && index < SCREEN_WIDTH)
									{
										if(BUF->SECTION_NAME[index-sindex] != 32)
											SPRITE[index][0]->PIX = BUF->SECTION_NAME[index-sindex];
										else
											SPRITE[index][0]->PIX = BACKGROUND;
										for(int windex = 1; windex < MENU_WIDTH; windex++)
											SPRITE[index][windex]->PIX = BUF->BACKGROUND;
										SPRITE[index][0]->hOWNER = BUF->SECTION_MENU;
										SPRITE[index][0]->OWNER_CODE = BUF->INDEX;
										SPRITE[index][0]->OWNER_TYPE = 8;
										index++;
									}
								if(index < SCREEN_WIDTH)
									{
										SPRITE[index][0]->PIX = BACKGROUND;
										SPRITE[index][0]->hOWNER = NULL;
										SPRITE[index][0]->OWNER_CODE = 0;
										SPRITE[index][0]->OWNER_TYPE = 0;
										for(int windex = 1; windex < MENU_WIDTH; windex++)
											SPRITE[index][windex]->PIX = BACKGROUND;
									}
								index++;
								BUF = BUF->NEXT;
							}
						break;
					}
				default:
					{
						while(BUF)
							{
								sindex = index;
								if(index < SCREEN_WIDTH)
									BUF->POS = index;
								else
									BUF->POS = -1;
								while(index-sindex < strlen(BUF->SECTION_NAME) && index < SCREEN_WIDTH)
									{
										if(BUF->SECTION_NAME[index-sindex] != 32)
											SPRITE[index][HEIGHT-1]->PIX = BUF->SECTION_NAME[index-sindex];
										else
											SPRITE[index][HEIGHT-1]->PIX = BACKGROUND;
										for(int windex = HEIGHT-2; windex >= 0; windex--)
											SPRITE[index][windex]->PIX = BUF->BACKGROUND;
										SPRITE[index][HEIGHT-1]->hOWNER = BUF->SECTION_MENU;
										SPRITE[index][HEIGHT-1]->OWNER_CODE = BUF->INDEX;
										SPRITE[index][HEIGHT-1]->OWNER_TYPE = 8;
										index++;
									}
								if(index < SCREEN_WIDTH)
									{
										SPRITE[index][HEIGHT-1]->PIX = BACKGROUND;
										SPRITE[index][HEIGHT-1]->hOWNER = NULL;
										SPRITE[index][HEIGHT-1]->OWNER_CODE = 0;
										SPRITE[index][HEIGHT-1]->OWNER_TYPE = 0;
										for(int windex = HEIGHT-2; windex >= 0; windex--)
											SPRITE[index][windex]->PIX = BACKGROUND;
									}
								index++;
								BUF = BUF->NEXT;
							}
					}
			}
		if(SHOW && hAPP->IN_PROCESS())
			DYNAMIC_REDRAW(X, Y, REDRAW, true);
		else
			DYNAMIC_REDRAW(X, Y, REDRAW, false);
	}

bool TCMainMenu::CHECK(int Mx, int My) 
	{
		switch(ALIGN)
			{
				case C_LEFT:
					{
						if(SPRITE[MENU_WIDTH-1][My]->hOWNER)
							{
								Mx = SPRITE[MENU_WIDTH-1][My]->OWNER_CODE;
								while(SPRITE[MENU_WIDTH-1][My]->OWNER_CODE == Mx && My-- > 0);
								((TCSubMenu*)(SPRITE[MENU_WIDTH-1][++My]->hOWNER))->DYNAMIC_REDRAW(MENU_WIDTH, My, REDRAW, true);
								((TCArrow*)(hAPP->ARROW))->SELECTED = (TCObject*)(SPRITE[MENU_WIDTH-1][My]->hOWNER);
								((TCArrow*)(hAPP->ARROW))->REACT(Mx, My, 8);
								return true;
							}
						return false;
					}
				case C_RIGHT:
					{
						if(SPRITE[0][My]->hOWNER)
							{
								Mx = SPRITE[0][My]->OWNER_CODE;
								while(SPRITE[0][My]->OWNER_CODE == Mx && My-- > 0);
								((TCSubMenu*)(SPRITE[0][++My]->hOWNER))->DYNAMIC_REDRAW(SCREEN_WIDTH-MENU_WIDTH-((TCSubMenu*)(SPRITE[0][My]->hOWNER))->GET_WIDTH(), My, REDRAW, true);
								((TCArrow*)(hAPP->ARROW))->SELECTED = (TCObject*)(SPRITE[0][My]->hOWNER);
								((TCArrow*)(hAPP->ARROW))->REACT(Mx, My, 8);
								return true;
							}
						return false;
					}
				case C_TOP:
					{
						if(SPRITE[Mx][MENU_WIDTH-1]->hOWNER)
							{
								My = SPRITE[Mx][MENU_WIDTH-1]->OWNER_CODE;
								while(SPRITE[Mx][MENU_WIDTH-1]->OWNER_CODE == My && Mx-- > 0);
								((TCSubMenu*)(SPRITE[++Mx][MENU_WIDTH-1]->hOWNER))->DYNAMIC_REDRAW(Mx, MENU_WIDTH, REDRAW, true);
								((TCArrow*)(hAPP->ARROW))->SELECTED = (TCObject*)(SPRITE[Mx][MENU_WIDTH-1]->hOWNER);
								((TCArrow*)(hAPP->ARROW))->REACT(Mx, My, 8);
								return true;
							}
						return false;
					}
				default:
					{
						if(SPRITE[Mx][0]->hOWNER)
							{
								My = SPRITE[Mx][0]->OWNER_CODE;
								while(SPRITE[Mx][0]->OWNER_CODE == My && Mx-- > 0);
								((TCSubMenu*)(SPRITE[++Mx][0]->hOWNER))->DYNAMIC_REDRAW(Mx, SCREEN_HEIGHT-MENU_WIDTH-((TCSubMenu*)(SPRITE[Mx][0]->hOWNER))->GET_HEIGHT(), REDRAW, true);
								((TCArrow*)(hAPP->ARROW))->SELECTED = (TCObject*)(SPRITE[Mx][0]->hOWNER);
								((TCArrow*)(hAPP->ARROW))->REACT(Mx, My, 8);
								return true;
							}
						return false;
					}
			} 
	}

void TCMainMenu::REACT(int Mx, int My, WORD ACTION_TYPE)
	{
		switch (ACTION_TYPE)
			{
				case C_MMOVE: 
					{
						if(ACTION_ON_MOUSE_MOVE)
							ACTION_ON_MOUSE_MOVE(this, Mx, My);
						return;
					}
				case C_MDOWNL:
					{
						if(ENABLED)
							{
								if(!CHECK(Mx, My))
									((TCArrow*)(hAPP->ARROW))->SELECTED = this;
							}
						if(ACTION_ON_MOUSE_DOWN)	
							ACTION_ON_MOUSE_DOWN(this, Mx, My);
						return;
					}
				case C_MDOWNR:
					{
						if(ACTION_ON_MOUSE_DOWN_R)
							ACTION_ON_MOUSE_DOWN_R(this, Mx, My);
						return;
					}
				case C_MUP:
					{
						if(ACTION_ON_MOUSE_UP) 
							ACTION_ON_MOUSE_UP(this, Mx, My, ((TCArrow*)(hAPP->ARROW))->LAST_PRESSED);
						return;
					}
				case C_MDBCLICK:
					{
						if(ACTION_ON_MOUSE_DBCLICK)
							ACTION_ON_MOUSE_DBCLICK(this, Mx, My, ((TCArrow*)(hAPP->ARROW))->LAST_PRESSED);
						return;
					}
				case C_MSCROLL_DOWN:
					{
						if(ENABLED)
							{
								MOVE_UP();
								((TCArrow*)(hAPP->ARROW))->SELECTED = this;
								((TCArrow*)(hAPP->ARROW))->REACT(Mx, My, 8);
							}
						if(ACTION_ON_SCROLL_UP)
							ACTION_ON_SCROLL_UP(this, Mx, My);
						return;
					}
				case C_MSCROLL_UP:
					{
						if(ENABLED)
							{
								MOVE_DOWN();
								((TCArrow*)(hAPP->ARROW))->SELECTED = this;
								((TCArrow*)(hAPP->ARROW))->REACT(Mx, My, 8);
							}
						if(ACTION_ON_SCROLL_DOWN)
							ACTION_ON_SCROLL_DOWN(this, Mx, My);
						return;
					}
				case C_SELECT:
					{
						if(ACTION_ON_SELECT)
							ACTION_ON_SELECT(this, Mx, My);
						return;
					}
				case C_UNSELECT:
					{
						if(ACTION_ON_UNSELECT)
							ACTION_ON_UNSELECT(this, Mx, My);
						return;
					}
			}
	}

//************************************************************************************************
//******** TCButton
//************************************************************************************************

TCButton::TCButton(unsigned new_WIDTH, unsigned new_HEIGHT, int new_X, int new_Y, char in_BACKGROUND, char in_SHADOW, TCObject* in_PARENT) : TCComponent(WDH_HGH_RET(new_WIDTH), WDH_HGH_RET(new_HEIGHT), new_X, new_Y, in_BACKGROUND, CBUTTON, in_PARENT)
	{
		SP_INIT(in_BACKGROUND, in_SHADOW);
		for (register int yindex = 0; yindex < (int)(HEIGHT); yindex++)
			{
				for (int xindex = 0; xindex < (int)(WIDTH); xindex++)
					SPRITE[xindex][yindex]->PIX = RELEASED_SP[xindex][yindex];
			}
		CAP_X = 0;
		CAP_Y = 0;
		if(hAPP->IN_PROCESS())
			DYNAMIC_REDRAW(new_X, new_Y, DRAW, true);
		else
			DYNAMIC_REDRAW(new_X, new_Y, DRAW, false);
	}

TCButton::~TCButton()
	{
		for(register int index = 0; index < (int)(WIDTH); index++)
			{
				delete[] PUSHED_SP[index];
				delete[] RELEASED_SP[index];
			}
		delete[] PUSHED_SP;
		delete[] RELEASED_SP;
	}

void TCButton::SP_INIT(char in_BACKGROUND, char in_SHADOW)
	{
		RELEASED_SP = new char*[WIDTH];
		PUSHED_SP = new char*[WIDTH];

		for (register int index = 0; index < (int)(WIDTH); index++)
			{
				RELEASED_SP[index] = new char[HEIGHT];
				PUSHED_SP[index] = new char[HEIGHT];
			}

		for (register int yindex = 0; yindex < (int)(HEIGHT)-1; yindex++)
			{
				for(int xindex = 0; xindex < (int)(WIDTH)-1; xindex++)
					RELEASED_SP[xindex][yindex] = in_BACKGROUND;
			}

		for(register int index = 1; index < (int)(WIDTH); index++)
			RELEASED_SP[index][HEIGHT-1]  = in_SHADOW;

		for(register int index = 1; index < (int)(HEIGHT)-1; index++)
			RELEASED_SP[WIDTH-1][index] = in_SHADOW;

		RELEASED_SP[WIDTH-1][0]  = NULL;
		RELEASED_SP[0][HEIGHT-1] = NULL;

		for (register int yindex = 1; yindex < (int)(HEIGHT); yindex++)
			{
				for(int xindex = 1; xindex < (int)(WIDTH); xindex++)
					PUSHED_SP[xindex][yindex] = in_BACKGROUND;
			}

		for (register int index = 0; index < (int)(WIDTH); index++)
			PUSHED_SP[index][0] = NULL;
		for (register int index = 1; index < (int)(HEIGHT); index++)
			PUSHED_SP[0][index] = NULL;
	}

void TCButton::SP_LOAD_PUSHED()
	{
		for(register int yindex = 0; yindex < (int)(HEIGHT); yindex++)
			{
				for(register int xindex = 0; xindex < (int)(WIDTH); xindex++)
					SPRITE[xindex][yindex]->PIX = PUSHED_SP[xindex][yindex];
			}
		if(hAPP->IN_PROCESS())
			STATIC_REDRAW(true);
			//DYNAMIC_REDRAW(X, Y, REDRAW, true);
		else
			STATIC_REDRAW(false);
			//DYNAMIC_REDRAW(X, Y, REDRAW, false);
	}

void TCButton::SP_LOAD_RELEASED()
	{
		for(register int yindex = 0; yindex < (int)(HEIGHT); yindex++)
			{
				for(register int xindex = 0; xindex < (int)(WIDTH); xindex++)
					SPRITE[xindex][yindex]->PIX = RELEASED_SP[xindex][yindex];
			}
		if(hAPP->IN_PROCESS())
			STATIC_REDRAW(true);
			//DYNAMIC_REDRAW(X, Y, REDRAW, true);
		else
			STATIC_REDRAW(false);
			//DYNAMIC_REDRAW(X, Y, REDRAW, false);
	}

void TCButton::SET_CAPTION(const char* new_CAPTION, int x, int y)
	{
		if(new_CAPTION)
			{
				if(CAPTION && CAP_Y < (int)(HEIGHT)-1 && CAP_Y >= 0 && CAP_X < (int)(WIDTH)-1 && CAP_X >= 0)
					{
						for(register int index = CAP_X; index-CAP_X < (int)(strlen(CAPTION)) && index < (int)(WIDTH)-1; index++)
							{
								SPRITE[index][CAP_Y]->PIX = BACKGROUND;
								RELEASED_SP[index][CAP_Y] = BACKGROUND;
								PUSHED_SP[index+1][CAP_Y+1] = BACKGROUND;
							}
					}
				CAPTION = new_CAPTION;
				CAP_X = x;
				CAP_Y = y;
				if(y < (int)(HEIGHT)-1 && y >= 0 && x < (int)(WIDTH)-1 && x >= 0)
					{
						for (int index = x; index-x < (int)(strlen(CAPTION)) && index < (int)(WIDTH)-1; index++)
							{
								SPRITE[index][y]->PIX = CAPTION[index-x];
								RELEASED_SP[index][y] = CAPTION[index-x];
								PUSHED_SP[index+1][y+1] = CAPTION[index-x];
							}
					}
			}
		if(hAPP->IN_PROCESS())
			STATIC_REDRAW(true);
		else
			STATIC_REDRAW(false);
	}

const char* TCButton::GET_CAPTION()
	{
		return CAPTION;
	}

void TCButton::REACT(int Mx, int My, WORD ACTION_TYPE)
	{
		switch (ACTION_TYPE)
			{
				case C_MMOVE: 
					{
						if(ACTION_ON_MOUSE_MOVE)
							ACTION_ON_MOUSE_MOVE(this, Mx, My);
						return;
					}
				case C_MDOWNL:
					{
						if(ENABLED)
							{
								SP_LOAD_PUSHED();
								((TCArrow*)(hAPP->ARROW))->CLICKED = this;
								((TCArrow*)(hAPP->ARROW))->SELECTED = this;
							}
						if(ACTION_ON_MOUSE_DOWN)	
							ACTION_ON_MOUSE_DOWN(this, Mx, My);
						return;
					}
				case C_MDOWNR:
					{
						if(ENABLED && MENU)
							{
								((TCObject*)(MENU))->DYNAMIC_REDRAW(Mx+PARENT->GET_X(), My+PARENT->GET_Y(), REDRAW, true);
								((TCArrow*)(hAPP->ARROW))->SELECTED = MENU;
								((TCArrow*)(hAPP->ARROW))->REACT(Mx, My, 8);
							}
						if(ACTION_ON_MOUSE_DOWN_R)
							ACTION_ON_MOUSE_DOWN_R(this, Mx, My);
						return;
					}
				case C_MUP:
					{
						if(ENABLED)
							{
								if(((TCArrow*)(hAPP->ARROW))->LAST_PRESSED == FROM_LEFT_1ST_BUTTON_PRESSED)
									{
										SP_LOAD_RELEASED();
										hAPP->MAIN_SCR->REDRAW();
									}
							}
						if(ACTION_ON_MOUSE_UP) 
							ACTION_ON_MOUSE_UP(this, Mx, My, ((TCArrow*)(hAPP->ARROW))->LAST_PRESSED);
						return;
					}
				case C_MDBCLICK:
					{
						if(ACTION_ON_MOUSE_DBCLICK)
							ACTION_ON_MOUSE_DBCLICK(this, Mx, My, ((TCArrow*)(hAPP->ARROW))->LAST_PRESSED);
						return;
					}
				case C_MSCROLL_DOWN:
					{
						if(ACTION_ON_SCROLL_UP)
							ACTION_ON_SCROLL_UP(this, Mx, My);
						return;
					}
				case C_MSCROLL_UP:
					{
						if(ACTION_ON_SCROLL_DOWN)
							ACTION_ON_SCROLL_DOWN(this, Mx, My);
						return;
					}
				case C_SELECT:
					{
						if(ACTION_ON_SELECT)
							ACTION_ON_SELECT(this, Mx, My);
						return;
					}
				case C_UNSELECT:
					{
						if(ACTION_ON_UNSELECT)
							ACTION_ON_UNSELECT(this, Mx, My);
						return;
					}
			}
	}

//************************************************************************************************
//******** TCLabel
//************************************************************************************************

TCLabel::TCLabel(unsigned new_WIDTH, unsigned new_HEIGHT, int new_X, int new_Y, char in_BACKGROUND, const char* in_CAPTION, int in_CAP_X, int in_CAP_Y, TCObject* in_PARENT) : TCComponent(new_WIDTH, new_HEIGHT, new_X, new_Y, in_BACKGROUND, CLABEL, in_PARENT)
	{
		SET_CAPTION(in_CAPTION, in_CAP_X, in_CAP_Y);
		if(hAPP->IN_PROCESS())
			DYNAMIC_REDRAW(new_X, new_Y, DRAW, true);
		else
			DYNAMIC_REDRAW(new_X, new_Y, DRAW, false);
	}

TCLabel::TCLabel(unsigned new_WIDTH, unsigned new_HEIGHT, int new_X, int new_Y, char in_BACKGROUND, TCObject* in_PARENT) : TCComponent(new_WIDTH, new_HEIGHT, new_X, new_Y, in_BACKGROUND, CLABEL, in_PARENT)
	{
		CAPTION = NULL;
		CAP_X = 0;
		CAP_Y = 0;
		if(hAPP->IN_PROCESS())
			DYNAMIC_REDRAW(new_X, new_Y, DRAW, true);
		else
			DYNAMIC_REDRAW(new_X, new_Y, DRAW, false);
	}

void TCLabel::SET_CAPTION(const char* new_CAPTION, int x, int y)
	{
		if(new_CAPTION)
			{
				if(CAPTION && CAP_Y < (int)(HEIGHT)-1 && CAP_Y >= 0 && CAP_X < (int)(WIDTH)-1 && CAP_X >= 0)
					{
						for(int index = CAP_X; index-CAP_X < (int)(strlen(CAPTION)) && index < (int)(WIDTH)-1; index++)
							SPRITE[index][CAP_Y]->PIX = BACKGROUND;
					}
				CAPTION = new_CAPTION;
				CAP_X = x;
				CAP_Y = y;
				if(y < (int)(HEIGHT)-1 && y >= 0 && x < (int)(WIDTH)-1 && x >= 0)
					{
						for (int index = x; index-x < (int)(strlen(CAPTION)) && index < (int)(WIDTH)-1; index++)
							SPRITE[index][y]->PIX = CAPTION[index-x];
					}
			}
		if(hAPP->IN_PROCESS())
			STATIC_REDRAW(true);
		else
			STATIC_REDRAW(false);
	}

const char* TCLabel::GET_CAPTION()
	{
		return CAPTION;
	}

void TCLabel::REACT(int Mx, int My, WORD ACTION_TYPE)
	{
		switch (ACTION_TYPE)
			{
				case C_MMOVE: 
					{
						if(ACTION_ON_MOUSE_MOVE)
							ACTION_ON_MOUSE_MOVE(this, Mx, My);
						return;
					}
				case C_MDOWNL:
					{
						if(ENABLED)
							{
								((TCArrow*)(hAPP->ARROW))->SELECTED = this;
								PARENT->DYNAMIC_REDRAW(PARENT->GET_X(), PARENT->GET_Y(), REDRAW, true);
							}
						if(ACTION_ON_MOUSE_DOWN)	
							ACTION_ON_MOUSE_DOWN(this, Mx, My);
						return;
					}
				case C_MDOWNR:
					{
						if(ENABLED && MENU)
							{
								((TCObject*)(MENU))->DYNAMIC_REDRAW(Mx+PARENT->GET_X(), My+PARENT->GET_Y(), REDRAW, true);
								((TCArrow*)(hAPP->ARROW))->SELECTED = MENU;
								((TCArrow*)(hAPP->ARROW))->REACT(Mx, My, 8);
							}
						if(ACTION_ON_MOUSE_DOWN_R)
							ACTION_ON_MOUSE_DOWN_R(this, Mx, My);
						return;
					}
				case C_MUP:
					{
						if(ACTION_ON_MOUSE_UP) 
							ACTION_ON_MOUSE_UP(this, Mx, My, ((TCArrow*)(hAPP->ARROW))->LAST_PRESSED);
						return;
					}
				case C_MDBCLICK:
					{
						if(ACTION_ON_MOUSE_DBCLICK)
							ACTION_ON_MOUSE_DBCLICK(this, Mx, My, ((TCArrow*)(hAPP->ARROW))->LAST_PRESSED);
						return;
					}
				case C_MSCROLL_UP:
					{
						if(ACTION_ON_SCROLL_UP)
							ACTION_ON_SCROLL_UP(this, Mx, My);
						return;
					}
				case C_MSCROLL_DOWN:
					{
						if(ACTION_ON_SCROLL_DOWN)
							ACTION_ON_SCROLL_DOWN(this, Mx, My);
						return;
					}
				case C_SELECT:
					{
						if(ACTION_ON_SELECT)
							ACTION_ON_SELECT(this, Mx, My);
						return;
					}
				case C_UNSELECT:
					{
						if(ACTION_ON_UNSELECT)
							ACTION_ON_UNSELECT(this, Mx, My);
						return;
					}
			}
	}

//************************************************************************************************
//******** TCMemo
//************************************************************************************************

TCMemo::TCMemo(unsigned new_WIDTH, unsigned new_HEIGHT, unsigned new_BUF_WIDTH, unsigned new_BUF_HEIGHT, int new_X, int new_Y, char in_BACKGROUND, TCObject* in_PARENT) : TCComponent(WDH_HGH_RET(new_WIDTH), WDH_HGH_RET(new_HEIGHT), new_X, new_Y, in_BACKGROUND, CMEMO, in_PARENT)
	{
		
		ACTION_ON_CHANGE = NULL;

		EOT = 0;
		INSERT_MODE = false;
		BUF_WIDTH = new_BUF_WIDTH;
		BUF_HEIGHT = new_BUF_HEIGHT;
		if(BUF_WIDTH >= (int)(WIDTH))
			AVAILABLE_WIDTH = WIDTH-2;
		else
			AVAILABLE_WIDTH = BUF_WIDTH;
		if(BUF_HEIGHT >= (int)(HEIGHT))
			AVAILABLE_HEIGHT = HEIGHT-2;
		else
			AVAILABLE_HEIGHT = BUF_HEIGHT;
		BUF_X = 0;
		BUF_Y = 0;
		TEXT_BUFFER = new char*[BUF_WIDTH];
		for(register int xindex = 0; xindex < BUF_WIDTH; xindex++)
			{
				TEXT_BUFFER[xindex] = new char[BUF_HEIGHT];
				for(int yindex = 0; yindex < BUF_HEIGHT; yindex++)
					TEXT_BUFFER[xindex][yindex] = BACKGROUND; 
			}
		CARET.PIX_UNDER = BACKGROUND;
		CARET.POS = 0;
		CARET.X = 0;
		CARET.Y = 0;
		CARET.STYLE = CARET_STYLE;
		for(register int index = 1; index < (int)(WIDTH)-1; index++)
			{
				SPRITE[index][0]->PIX = (TCHAR)(COMP_HORIZONTAL_2);
				SPRITE[index][HEIGHT-1]->PIX = (TCHAR)(COMP_HORIZONTAL_2);
			}
		for (register int index = 1; index < (int)(HEIGHT)-1; index++)
			{
				SPRITE[0][index]->PIX = (TCHAR)(COMP_VERTICAL_1);
				SPRITE[WIDTH-1][index]->PIX = (TCHAR)(COMP_VERTICAL_1);
			}
		SPRITE[0][0]->PIX = (TCHAR)(COMP_TOP_LEFT_2_1);
		SPRITE[WIDTH-1][0]->PIX = (TCHAR)(COMP_TOP_RIGHT_2_1);
		SPRITE[0][HEIGHT-1]->PIX = (TCHAR)(COMP_BOTTOM_LEFT_1_2);
		SPRITE[WIDTH-1][HEIGHT-1]->PIX = (TCHAR)(COMP_BOTTOM_RIGHT_1_2);
		if(hAPP->IN_PROCESS())
			DYNAMIC_REDRAW(new_X, new_Y, DRAW, true);
		else
			DYNAMIC_REDRAW(new_X, new_Y, DRAW, false);
	}

TCMemo::~TCMemo()
	{
		if(TEXT_BUFFER)
			{
				for(register int xindex = 0; xindex < BUF_WIDTH; xindex++)
					delete[] TEXT_BUFFER[xindex];
				delete[] TEXT_BUFFER;
			}
	}

void TCMemo::SELECT(bool SELECTED)
	{
		if(SELECTED)
			{
				CARET.PIX_UNDER = TEXT_BUFFER[CARET.X][CARET.Y];
				TEXT_BUFFER[CARET.X][CARET.Y] = CARET.STYLE;
			}
		else
			TEXT_BUFFER[CARET.X][CARET.Y] = CARET.PIX_UNDER;
		SHOW_BUFFER();
	}

void TCMemo::REACT(int Mx, int My, WORD ACTION_TYPE)
	{
		switch (ACTION_TYPE)
			{
				case C_MMOVE: 
					{
						if(ACTION_ON_MOUSE_MOVE)
							ACTION_ON_MOUSE_MOVE(this, Mx, My);
						return;
					}
				case C_KEY_INPUT:
					{
						if(ENABLED)
							{
								  INPUT(((TCArrow*)(hAPP->ARROW))->INPUT_KEY, ((TCArrow*)(hAPP->ARROW))->CONTROL_KEY_STATE, true);
								  TEXT_BUFFER[CARET.X][CARET.Y] = CARET.PIX_UNDER;
							}
						  if(ACTION_ON_CHANGE)
							  ACTION_ON_CHANGE(this, Mx, My);
						  return;
					}
				case C_MDOWNL:
					{
						if(ENABLED)
							{
								((TCArrow*)(hAPP->ARROW))->SELECTED = this;
								PARENT->DYNAMIC_REDRAW(PARENT->GET_X(), PARENT->GET_Y(), REDRAW, true);
								((TCArrow*)(hAPP->ARROW))->CLICKED = this;
							}
						if(ACTION_ON_MOUSE_DOWN)	
							ACTION_ON_MOUSE_DOWN(this, Mx, My);
						return;
					}
				case C_MDOWNR:
					{
						if(ENABLED && MENU)
							{
								((TCObject*)(MENU))->DYNAMIC_REDRAW(Mx+PARENT->GET_X(), My+PARENT->GET_Y(), REDRAW, true);
								((TCArrow*)(hAPP->ARROW))->SELECTED = MENU;
								((TCArrow*)(hAPP->ARROW))->REACT(Mx, My, 8);
							}
						if(ACTION_ON_MOUSE_DOWN_R)
							ACTION_ON_MOUSE_DOWN_R(this, Mx, My);
						return;
					}
				case C_MUP:
					{
						if(ACTION_ON_MOUSE_UP) 
							ACTION_ON_MOUSE_UP(this, Mx, My, ((TCArrow*)(hAPP->ARROW))->LAST_PRESSED);
						return;
					}
				case C_MDBCLICK:
					{
						if(ACTION_ON_MOUSE_DBCLICK)
							ACTION_ON_MOUSE_DBCLICK(this, Mx, My, ((TCArrow*)(hAPP->ARROW))->LAST_PRESSED);
						return;
					}
				case C_MSCROLL_UP:
					{
						if(ACTION_ON_SCROLL_UP)
							ACTION_ON_SCROLL_UP(this, Mx, My);
						return;
					}
				case C_MSCROLL_DOWN:
					{
						if(ACTION_ON_SCROLL_DOWN)
							ACTION_ON_SCROLL_DOWN(this, Mx, My);
						return;
					}
				case C_SELECT:
					{
						if(ENABLED)
							SELECT(true);
						if(ACTION_ON_SELECT)
							ACTION_ON_SELECT(this, Mx, My);
						return;
					}
				case C_UNSELECT:
					{
						if(ENABLED)
							SELECT(false);
						if(ACTION_ON_UNSELECT)
							ACTION_ON_UNSELECT(this, Mx, My);
						return;
					}
			}
	}

void TCMemo::SHOW_BUFFER()
	{
		for(register int yindex = 1; yindex <= AVAILABLE_HEIGHT; yindex++)
			{
				for(register int xindex = 1; xindex <= AVAILABLE_WIDTH; xindex++)
					SPRITE[xindex][yindex]->PIX = TEXT_BUFFER[xindex-1+BUF_X][yindex-1+BUF_Y];
			}
		if(hAPP->IN_PROCESS())
			STATIC_REDRAW(true);
		else
			STATIC_REDRAW(false);
	}

char TCMemo::TO_ADDITIONAL(char SINGLE)
	{
		if(SINGLE >= 65 && SINGLE <= 93)
			return SINGLE+32;
		else
			{
				switch(SINGLE)
					{
						case 32: return BACKGROUND;
						case 44: return 60;
						case 45: return 95;
						case 46: return 62;
						case 47: return 63;
						case 48: return 41;
						case 49: return 33;
						case 50: return 64;
						case 51: return 35;
						case 52: return 36;
						case 53: return 37;
						case 54: return 94;
						case 55: return 38;
						case 56: return 42;
						case 57: return 40;
						case 61: return 43;
						default: return  0;
					}
			}
	}

void TCMemo::INPUT(WORD INPUT_SYMBOL, DWORD CONTROL_KEY, bool SHOW_CARET)
	{
		if(SHOW_CARET)
			TEXT_BUFFER[CARET.X][CARET.Y] = CARET.PIX_UNDER;
		if(/*CONTROL_KEY == 256 || CONTROL_KEY == 384 || CONTROL_KEY == 272 ||*/CONTROL_KEY == ARROW_KEY_SIMPLE || CONTROL_KEY == ARROW_KEY_CAPS || CONTROL_KEY == ARROW_KEY_SHIFT || CONTROL_KEY == ARROW_KEY_SHIFT_CAPS)
			{
				switch (INPUT_SYMBOL)
					{
						case 37://37 - left 38- up 39 - right 40 - down
							{
								if(CARET.POS-1 >= 0)
									CARET.POS--;
								break;
							}
						case 38:
							{
								if (CARET.POS-BUF_WIDTH >= 0)
									CARET.POS -= BUF_WIDTH;
								break;
							}
						case 39:
							{
								if(CARET.POS+1 < BUF_WIDTH*BUF_HEIGHT && CARET.POS+1 <= (int)(EOT))
									CARET.POS++;
								break;
							}
						case 40:
							{
								if (CARET.POS+BUF_WIDTH < BUF_WIDTH*BUF_HEIGHT && CARET.POS+BUF_WIDTH <= (int)(EOT))
									CARET.POS += BUF_WIDTH;
								break;
							}
						case 46:	//delete
							{
								if(CARET.POS < (int)(EOT) && EOT > 0)
									{
										int pindex = CARET.POS-1;
										while(++pindex < (int)(EOT)-1)
											TEXT_BUFFER[pindex%BUF_WIDTH][pindex/BUF_WIDTH] = TEXT_BUFFER[(pindex+1)%BUF_WIDTH][(pindex+1)/BUF_WIDTH];
										TEXT_BUFFER[pindex%BUF_WIDTH][pindex/BUF_WIDTH] = BACKGROUND;
										EOT--;
									}
							}
					}
			}
		else
			{
				if(INPUT_SYMBOL == 8 && EOT > 0 && CARET.POS > 0) //del
					{
						for(int pindex = CARET.POS-1; pindex < (int)(EOT); pindex++)
							TEXT_BUFFER[pindex%BUF_WIDTH][pindex/BUF_WIDTH] = TEXT_BUFFER[(pindex+1)%BUF_WIDTH][(pindex+1)/BUF_WIDTH];
						EOT--;
						CARET.POS--;
					}
				else if (INPUT_SYMBOL == 13 && (int)(EOT) + BUF_WIDTH-CARET.X < BUF_WIDTH*BUF_HEIGHT) //enter
					{
						EOT += BUF_WIDTH-CARET.X;
						for(int pindex = CARET.POS+(BUF_WIDTH-CARET.X); pindex < (int)(EOT); pindex++)
							TEXT_BUFFER[pindex%BUF_WIDTH][pindex/BUF_WIDTH] = TEXT_BUFFER[(pindex-BUF_WIDTH)%BUF_WIDTH][(pindex-BUF_WIDTH)/BUF_WIDTH];
						for(int pindex = CARET.POS; pindex < CARET.POS+(BUF_WIDTH-CARET.X); pindex++)
							TEXT_BUFFER[pindex%BUF_WIDTH][pindex/BUF_WIDTH] = BACKGROUND;
						CARET.POS += BUF_WIDTH-CARET.X;
					}
				else if(INPUT_SYMBOL > 31 && INPUT_SYMBOL <= 126)
					{
						if((int)(EOT) < (BUF_WIDTH)*(BUF_HEIGHT))
							{
								EOT++;
								if(/*CONTROL_KEY != CAPSLOCK_ON && CONTROL_KEY != SHIFT_PRESSED &&*/ CONTROL_KEY == LARGE_LETTER_SHIFT || CONTROL_KEY == LARGE_LETTER_CAPS)
									{
										if(INPUT_SYMBOL < 65 || INPUT_SYMBOL > 93)
											INPUT_SYMBOL = TO_ADDITIONAL(INPUT_SYMBOL);
									}
								else if(INPUT_SYMBOL >= 65 && INPUT_SYMBOL <= 93)
									INPUT_SYMBOL = TO_ADDITIONAL(INPUT_SYMBOL);
								if(!INSERT_MODE)
									{
										for(int pindex = EOT; pindex > CARET.POS; pindex--)
											TEXT_BUFFER[pindex%BUF_WIDTH][pindex/BUF_WIDTH] = TEXT_BUFFER[(pindex-1)%BUF_WIDTH][(pindex-1)/BUF_WIDTH];
									}
								TEXT_BUFFER[CARET.X][CARET.Y] = (char)(INPUT_SYMBOL);
								if(CARET.POS+1 < BUF_WIDTH*BUF_HEIGHT)
									CARET.POS++;
							}
						else
							{
								if(/*CONTROL_KEY != CAPSLOCK_ON && CONTROL_KEY != SHIFT_PRESSED &&*/ CONTROL_KEY != LARGE_LETTER_SHIFT && CONTROL_KEY != LARGE_LETTER_CAPS)
									INPUT_SYMBOL = TO_ADDITIONAL(INPUT_SYMBOL);
								TEXT_BUFFER[CARET.X][CARET.Y] = (char)(INPUT_SYMBOL);
							}
					}
			}
		CARET.X = CARET.POS % BUF_WIDTH;
		CARET.Y = CARET.POS / BUF_WIDTH;

		if(CARET.X >= BUF_X+(int)(WIDTH)-2)
			BUF_X = CARET.X-WIDTH+3;
		else if(CARET.X < BUF_X)
			BUF_X = CARET.X;
		if(CARET.Y >= BUF_Y+(int)(HEIGHT)-2)
			BUF_Y = CARET.Y-HEIGHT+3;
		else if( CARET.Y < BUF_Y)
			BUF_Y = CARET.Y;
		
		if(SHOW_CARET)
			{
				CARET.PIX_UNDER = TEXT_BUFFER[CARET.X][CARET.Y];
				TEXT_BUFFER[CARET.X][CARET.Y] = CARET.STYLE;
			}
		SHOW_BUFFER();
	}

void TCMemo::ADD_TEXT(const char* in_STR)
	{
		register int index = (int)(EOT);
		while(index-EOT < (int)(strlen(in_STR)) && index < BUF_WIDTH*BUF_HEIGHT)
			{
				TEXT_BUFFER[index%BUF_WIDTH][index/BUF_WIDTH] = in_STR[index-EOT];
				index++;
			}
		EOT = (unsigned)(index);
		SHOW_BUFFER();
	}

char* TCMemo::GET_TEXT()
	{
		char* ret_STR = new char[EOT+1];
		ret_STR[EOT] = NULL;
		for(register int index = 0; index < (int)(EOT); index++)
			ret_STR[index] = TEXT_BUFFER[index%BUF_WIDTH][index/BUF_WIDTH];
		return ret_STR;
	}

void TCMemo::CLEAR()
	{
		TEXT_BUFFER[CARET.X][CARET.Y] = BACKGROUND;
		for(register int index = 1; index < (int)(EOT); index++)
			TEXT_BUFFER[index%BUF_WIDTH][index/BUF_WIDTH] = BACKGROUND;
		CARET.X = 0;
		CARET.Y = 0;
		CARET.POS = 0;
		EOT = 0;
		CARET.PIX_UNDER = BACKGROUND;
		SELECT(false);
	}

void TCMemo::SET_TEXT(const char* new_TEXT)
	{
		CLEAR();
		ADD_TEXT(new_TEXT);
	}

//************************************************************************************************
//******** TCListBox
//************************************************************************************************

TCListBox::TCListBox(unsigned new_WIDTH, unsigned new_HEIGHT, int new_X, int new_Y, char in_BACKGROUND, TCObject* in_PARENT) : TCComponent(WDH_HGH_RET(new_WIDTH), WDH_HGH_RET(new_HEIGHT), new_X, new_Y, in_BACKGROUND, CLISTBOX, in_PARENT)
	{
		FIRST = NULL;
		LAST = NULL;
		SELECTED = NULL;
		COUNT = 0;
		for(register int index = 1; index < (int)(WIDTH)-1; index++)
			{
				SPRITE[index][0]->PIX = (TCHAR)(COMP_HORIZONTAL_1);
				SPRITE[index][HEIGHT-1]->PIX = (TCHAR)(COMP_HORIZONTAL_1);
			}
		for (register int index = 1; index < (int)(HEIGHT)-1; index++)
			{
				SPRITE[0][index]->PIX = (TCHAR)(COMP_VERTICAL_2);
				SPRITE[WIDTH-1][index]->PIX = (TCHAR)(COMP_VERTICAL_2);
			}
		SPRITE[0][0]->PIX = (TCHAR)(COMP_TOP_LEFT_1_2);
		SPRITE[WIDTH-1][0]->PIX = (TCHAR)(COMP_TOP_RIGHT_1_2);
		SPRITE[0][HEIGHT-1]->PIX = (TCHAR)(COMP_BOTTOM_LEFT_2_1);
		SPRITE[WIDTH-1][HEIGHT-1]->PIX = (TCHAR)(COMP_BOTTOM_RIGHT_2_1);
		if(hAPP->IN_PROCESS())
			DYNAMIC_REDRAW(new_X, new_Y, DRAW, true);
		else
			DYNAMIC_REDRAW(new_X, new_Y, DRAW, false);
	}

TCListBox::~TCListBox()
	{
		while(FIRST)
			{
				LAST = FIRST;
				FIRST = FIRST->NEXT;
				delete LAST;
			}
	}

void TCListBox::UPDATE_LIST()
	{
		CLIST_STRING* BUF = FIRST_DRAWED;
		int counter = 0;
		while(BUF && BUF->INDEX-FIRST_DRAWED->INDEX+1 < (int)(HEIGHT)-1)
			{
				if(!BUF->SELECTED)
					{
						for(register int index = 1; index < (int)(WIDTH)-1 && index < (int)(BUF->OFFSET); index++)
							SPRITE[index][BUF->INDEX-FIRST_DRAWED->INDEX+1]->PIX = BUF->UNSELECTED_STYLE;
						for (register int index = BUF->OFFSET; index < (int)(WIDTH)-1 && index-BUF->OFFSET < strlen(BUF->STRING); index++)
							SPRITE[index][BUF->INDEX-FIRST_DRAWED->INDEX+1]->PIX = BUF->STRING[index-BUF->OFFSET];
						for (register int index = BUF->OFFSET+strlen(BUF->STRING); index < (int)(WIDTH)-1; index++)
							SPRITE[index][BUF->INDEX-FIRST_DRAWED->INDEX+1]->PIX = BUF->UNSELECTED_STYLE;
					}
				else
					{
						for(register int index = 1; index < (int)(WIDTH)-1 && index < (int)(BUF->OFFSET); index++)
							SPRITE[index][BUF->INDEX-FIRST_DRAWED->INDEX+1]->PIX = BUF->SELECTED_STYLE;
						for (register int index = BUF->OFFSET; index < (int)(WIDTH)-1 && index-BUF->OFFSET < strlen(BUF->STRING); index++)
							SPRITE[index][BUF->INDEX-FIRST_DRAWED->INDEX+1]->PIX = BUF->STRING[index-BUF->OFFSET];
						for (register int index = BUF->OFFSET+strlen(BUF->STRING); index < (int)(WIDTH)-1; index++)
							SPRITE[index][BUF->INDEX-FIRST_DRAWED->INDEX+1]->PIX = BUF->SELECTED_STYLE;
					}
				counter++;
				BUF = BUF->NEXT;
			}
		for(register int yindex = counter+1; yindex < (int)(HEIGHT)-1; yindex++)
			{
				for(register int xindex = 1; xindex < (int)(WIDTH)-1; xindex++)
					SPRITE[xindex][yindex]->PIX = BACKGROUND;
			}
		if(hAPP->IN_PROCESS())
			STATIC_REDRAW(true);
		else
			STATIC_REDRAW(false);
	}

int TCListBox::GET_COUNT()
	{
		return COUNT;
	}

CLIST_STRING_INFO* TCListBox::GET_SELECTED_INFO()
	{
		if(SELECTED)
			{
				CLIST_STRING_INFO* RET = new CLIST_STRING_INFO;
				RET->OFFSET			 = &SELECTED->OFFSET;
				RET->SELECTED_STYLE	 = &SELECTED->SELECTED_STYLE;
				RET->UNSELECTED_STYLE = &SELECTED->UNSELECTED_STYLE;
				RET->STRING			 = &SELECTED->STRING;
				RET->INDEX			 = SELECTED->INDEX;
				return RET;
			}
		return NULL;
	}

CLIST_STRING* TCListBox::GET_BY_INDEX(int in_INDEX)
	{
		CLIST_STRING* BUF = FIRST;
		while(BUF)
			{
				if(BUF->INDEX == in_INDEX)
					return BUF;
				BUF = BUF->NEXT;
			}
		return NULL;
	}

CLIST_STRING_INFO* TCListBox::GET_INFO_BY_INDEX(int in_INDEX)
	{
		CLIST_STRING* BUF	 = GET_BY_INDEX(in_INDEX);
		if(BUF)
			{
				CLIST_STRING_INFO* RET = new CLIST_STRING_INFO;
				RET->OFFSET			 = &BUF->OFFSET;
				RET->STRING			 = &BUF->STRING;
				RET->SELECTED_STYLE	 = &BUF->SELECTED_STYLE;
				RET->UNSELECTED_STYLE = &BUF->UNSELECTED_STYLE;
				RET->INDEX			 = BUF->INDEX;
				return RET;
			}
		return NULL;
	}

void TCListBox::INSERT_AFTER(int in_INDEX, const char* in_STRING, char in_SELECTED_STYLE, char in_UNSELECTED_STYLE, unsigned in_OFFSET)
	{
		CLIST_STRING* BUF = GET_BY_INDEX(in_INDEX);
		CLIST_STRING* real_STRING = new CLIST_STRING;
		real_STRING->OFFSET = in_OFFSET;
		real_STRING->SELECTED_STYLE = in_SELECTED_STYLE;
		real_STRING->UNSELECTED_STYLE = in_UNSELECTED_STYLE;
		real_STRING->STRING = in_STRING;
		if(BUF)
			{
				real_STRING->SELECTED = false;
				real_STRING->PREV = BUF;
				real_STRING->NEXT = BUF->NEXT;
				if(BUF->NEXT)
					BUF->NEXT->PREV = real_STRING;
				BUF->NEXT = real_STRING;
				real_STRING->INDEX = BUF->INDEX+1;
				if(real_STRING->PREV == LAST)
					LAST = real_STRING;
				if(real_STRING == FIRST_DRAWED->PREV)
					FIRST_DRAWED = BUF;
				BUF = real_STRING->NEXT;
				while(BUF)
					{
						BUF->INDEX++;
						BUF = BUF->NEXT;
					}
				COUNT++;
				UPDATE_LIST();
			}
		else
			ADD_TO_START(in_STRING, in_SELECTED_STYLE, in_UNSELECTED_STYLE, in_OFFSET);
	}

void TCListBox::ERASE(int in_INDEX)
	{
		CLIST_STRING* BUF = GET_BY_INDEX(in_INDEX);
		if (BUF)
			{
				CLIST_STRING* BUF_1 = BUF->NEXT;
				if(BUF_1)
					BUF_1->PREV = BUF->PREV;
				if(BUF->PREV)
					BUF->PREV->NEXT = BUF_1;
				if(BUF == FIRST)
					FIRST = BUF->NEXT;
				if(BUF == LAST)
					LAST = BUF->PREV;
				if(BUF == SELECTED)
					SELECTED = NULL;
				if(BUF == FIRST_DRAWED)
					{
						if(BUF->NEXT)
							FIRST_DRAWED = BUF->NEXT;
						else
							FIRST_DRAWED = BUF->PREV;
					}
				delete BUF;
				while (BUF_1)
					{
						BUF_1->INDEX--;
						BUF_1 = BUF_1->NEXT;
					}
				COUNT--;
			}
		UPDATE_LIST();
	}

void TCListBox::ADD_TO_END(const char* in_STRING, char in_SELECTED_STYLE, char in_UNSELECTED_STYLE, unsigned in_OFFSET)
	{
		CLIST_STRING* real_STRING = new CLIST_STRING;
		real_STRING->OFFSET = in_OFFSET;
		real_STRING->SELECTED_STYLE = in_SELECTED_STYLE;
		real_STRING->UNSELECTED_STYLE = in_UNSELECTED_STYLE;
		real_STRING->STRING = in_STRING;
		real_STRING->PREV = LAST;
		real_STRING->SELECTED = false;
		real_STRING->NEXT = NULL;
		if(LAST)
			{
				LAST->NEXT = real_STRING;
				real_STRING->INDEX = LAST->INDEX+1;
			}
		else
			real_STRING->INDEX = 0;
		LAST = real_STRING;
		if(FIRST_DRAWED)
			{
				if(real_STRING == FIRST_DRAWED->PREV)
					FIRST_DRAWED = real_STRING;
			}
		else
			FIRST_DRAWED = real_STRING;
		if(!FIRST)
			FIRST = real_STRING;
		COUNT++;
		UPDATE_LIST();
	}

void TCListBox::ADD_TO_START(const char* in_STRING, char in_SELECTED_STYLE, char in_UNSELECTED_STYLE, unsigned in_OFFSET)
	{
		CLIST_STRING* real_STRING = new CLIST_STRING;
		real_STRING->OFFSET = in_OFFSET;
		real_STRING->SELECTED_STYLE = in_SELECTED_STYLE;
		real_STRING->UNSELECTED_STYLE = in_UNSELECTED_STYLE;
		real_STRING->STRING = in_STRING;
		real_STRING->NEXT = FIRST;
		real_STRING->SELECTED = false;
		real_STRING->PREV = NULL;
		real_STRING->INDEX = 0;
		if(FIRST)
			FIRST->PREV = real_STRING;
		FIRST = real_STRING;
		if(FIRST_DRAWED)
			{
				if(real_STRING == FIRST_DRAWED->PREV)
					FIRST_DRAWED = real_STRING;
			}
		else
			FIRST_DRAWED = real_STRING;
		if(!LAST)
			LAST = real_STRING;
		real_STRING = real_STRING->NEXT;
		while(real_STRING)
			{
				real_STRING->INDEX++;
				real_STRING = real_STRING->NEXT;
			}
		COUNT++;
		UPDATE_LIST();
	}

void TCListBox::REACT(int Mx, int My, WORD ACTION_TYPE)
	{
		switch (ACTION_TYPE)
			{
				case C_MMOVE: 
					{
						if(ACTION_ON_MOUSE_MOVE)
							ACTION_ON_MOUSE_MOVE(this, Mx, My);
						return;
					}
				case C_MDOWNL:
					{
						if(ENABLED)
							{
								((TCArrow*)(hAPP->ARROW))->SELECTED = this;
								CHECK(Mx, My);
								DYNAMIC_REDRAW(X, Y, REDRAW, true);
							}
						if(ACTION_ON_MOUSE_DOWN)	
							ACTION_ON_MOUSE_DOWN(this, Mx, My);
						return;
					}
				case C_MDOWNR:
					{
						if(ENABLED && MENU)
							{
								((TCObject*)(MENU))->DYNAMIC_REDRAW(Mx+PARENT->GET_X(), My+PARENT->GET_Y(), REDRAW, true);
								((TCArrow*)(hAPP->ARROW))->SELECTED = MENU;
								((TCArrow*)(hAPP->ARROW))->REACT(Mx, My, 8);
							}
						if(ACTION_ON_MOUSE_DOWN_R)
							ACTION_ON_MOUSE_DOWN_R(this, Mx, My);
						return;
					}
				case C_MUP:
					{
						if(ACTION_ON_MOUSE_UP) 
							ACTION_ON_MOUSE_UP(this, Mx, My, ((TCArrow*)(hAPP->ARROW))->LAST_PRESSED);
						return;
					}
				case C_MDBCLICK:
					{
						if(ACTION_ON_MOUSE_DBCLICK)
							ACTION_ON_MOUSE_DBCLICK(this, Mx, My, ((TCArrow*)(hAPP->ARROW))->LAST_PRESSED);
						return;
					}
				case C_MSCROLL_UP:
					{
						if(ENABLED)
							MOVE_DOWN();
						if(ACTION_ON_SCROLL_UP)
							ACTION_ON_SCROLL_UP(this, Mx, My);
						return;
					}
				case C_MSCROLL_DOWN:
					{
						if(ENABLED)
							MOVE_UP();
						if(ACTION_ON_SCROLL_DOWN)
							ACTION_ON_SCROLL_DOWN(this, Mx, My);
						return;
					}
				case C_SELECT:
					{
						if(ACTION_ON_SELECT)
							ACTION_ON_SELECT(this, Mx, My);
						return;
					}
				case C_UNSELECT:
					{
						if(ACTION_ON_UNSELECT)
							ACTION_ON_UNSELECT(this, Mx, My);
						return;
					}
			}
	}

void TCListBox::CHECK(int Mx, int My)
	{
		if(Mx-X > 0 && My-Y > 0 && Mx-X < (int)(WIDTH-1) && My-Y < (int)(HEIGHT-1))
			{
				if(FIRST_DRAWED)
					{
						if(My-Y <= COUNT-FIRST_DRAWED->INDEX)
							{
								if(SELECTED)
									SELECTED->SELECTED = false;
								SELECTED = GET_BY_INDEX(FIRST_DRAWED->INDEX+My-Y-1);
								SELECTED->SELECTED = true;
								UPDATE_LIST();
							}
						else
							{
								if(SELECTED)
									SELECTED->SELECTED = false;
								SELECTED = NULL;
								UPDATE_LIST();
							}
					}
			}
	}

void TCListBox::MOVE_DOWN()
	{
		if(FIRST_DRAWED->NEXT)
			{
				FIRST_DRAWED = FIRST_DRAWED->NEXT;
				UPDATE_LIST();
			}
	}

void TCListBox::MOVE_UP()
	{
		if(FIRST_DRAWED->PREV)
			{
				FIRST_DRAWED = FIRST_DRAWED->PREV;
				UPDATE_LIST();
			}
	}

//************************************************************************************************
//******** TCCheckBox
//************************************************************************************************

TCCheckBox::TCCheckBox(int new_X, int new_Y, char in_BACKGROUND, TCObject* in_PARENT) : TCComponent(3, 3, new_X, new_Y, in_BACKGROUND, CCHECKBOX, in_PARENT)
	{
		SPRITE[0][0]->PIX = (TCHAR)(COMP_TOP_LEFT_1_1);
		SPRITE[2][0]->PIX = (TCHAR)(COMP_TOP_RIGHT_1_1);
		SPRITE[0][2]->PIX = (TCHAR)(COMP_BOTTOM_LEFT_1_1);
		SPRITE[2][2]->PIX = (TCHAR)(COMP_BOTTOM_RIGHT_1_1);

		SPRITE[1][0]->PIX = (TCHAR)(COMP_HORIZONTAL_1);
		SPRITE[1][2]->PIX = (TCHAR)(COMP_HORIZONTAL_1);
		SPRITE[0][1]->PIX = (TCHAR)(COMP_VERTICAL_1);
		SPRITE[2][1]->PIX = (TCHAR)(COMP_VERTICAL_1);

		CHECKED_STATUS = false;
		if(hAPP->IN_PROCESS())
			DYNAMIC_REDRAW(new_X, new_Y, DRAW, true);
		else
			DYNAMIC_REDRAW(new_X, new_Y, DRAW, false);
	}

TCCheckBox::TCCheckBox(int new_X, int new_Y, char in_BACKGROUND, bool in_STATUS,TCObject* in_PARENT) : TCComponent(3, 3, new_X, new_Y, in_BACKGROUND, CCHECKBOX, in_PARENT)
	{
		SPRITE[0][0]->PIX = (TCHAR)(COMP_TOP_LEFT_1_1);
		SPRITE[2][0]->PIX = (TCHAR)(COMP_TOP_RIGHT_1_1);
		SPRITE[0][2]->PIX = (TCHAR)(COMP_BOTTOM_LEFT_1_1);
		SPRITE[2][2]->PIX = (TCHAR)(COMP_BOTTOM_RIGHT_1_1);

		SPRITE[1][0]->PIX = (TCHAR)(COMP_HORIZONTAL_1);
		SPRITE[1][2]->PIX = (TCHAR)(COMP_HORIZONTAL_1);
		SPRITE[0][1]->PIX = (TCHAR)(COMP_VERTICAL_1);
		SPRITE[2][1]->PIX = (TCHAR)(COMP_VERTICAL_1);
		
		if(in_STATUS)
			{
				SPRITE[1][1]->PIX = CHECKBOX_CHECKED;
				CHECKED_STATUS = true;
			}
		else
			CHECKED_STATUS = false;
		if(hAPP->IN_PROCESS())
			DYNAMIC_REDRAW(new_X, new_Y, DRAW, true);
		else
			DYNAMIC_REDRAW(new_X, new_Y, DRAW, false);
	}

bool TCCheckBox::CHECK(bool in_STATUS)
	{
		if(CHECKED_STATUS != in_STATUS)
			{
				CHECKED_STATUS = in_STATUS;
				if(CHECKED_STATUS)
					{
						SPRITE[1][1]->PIX = CHECKBOX_CHECKED;
						CHECKED_STATUS = true;
					}
				else
					{
						SPRITE[1][1]->PIX = BACKGROUND;
						CHECKED_STATUS = false;
					}
				if(hAPP->IN_PROCESS())
					STATIC_REDRAW(true);
				else
					STATIC_REDRAW(false);
				return true;
			}
		return false;
	}

bool TCCheckBox::CHECKED()
	{
		return CHECKED_STATUS;
	}

void TCCheckBox::REACT(int Mx, int My, WORD ACTION_TYPE)
	{
		switch (ACTION_TYPE)
			{
				case C_MMOVE: 
					{
						if(ACTION_ON_MOUSE_MOVE)
							ACTION_ON_MOUSE_MOVE(this, Mx, My);
						return;
					}
				case C_MDOWNL:
					{
						if(ENABLED)
							{
								((TCArrow*)(hAPP->ARROW))->SELECTED = this;
								if(!CHECK(!CHECKED_STATUS))
									DYNAMIC_REDRAW(X, Y, REDRAW, true);
							}
						if(ACTION_ON_MOUSE_DOWN)	
							ACTION_ON_MOUSE_DOWN(this, Mx, My);
						return;
					}
				case C_MDOWNR:
					{
						if(ENABLED && MENU)
							{
								((TCObject*)(MENU))->DYNAMIC_REDRAW(Mx+PARENT->GET_X(), My+PARENT->GET_Y(), REDRAW, true);
								((TCArrow*)(hAPP->ARROW))->SELECTED = MENU;
								((TCArrow*)(hAPP->ARROW))->REACT(Mx, My, 8);
							}
						if(ACTION_ON_MOUSE_DOWN_R)
							ACTION_ON_MOUSE_DOWN_R(this, Mx, My);
						return;
					}
				case C_MUP:
					{
						if(ACTION_ON_MOUSE_UP) 
							ACTION_ON_MOUSE_UP(this, Mx, My, ((TCArrow*)(hAPP->ARROW))->LAST_PRESSED);
						return;
					}
				case C_MDBCLICK:
					{
						if(ACTION_ON_MOUSE_DBCLICK)
							ACTION_ON_MOUSE_DBCLICK(this, Mx, My, ((TCArrow*)(hAPP->ARROW))->LAST_PRESSED);
						return;
					}
				case C_MSCROLL_UP:
					{
						if(ACTION_ON_SCROLL_UP)
							ACTION_ON_SCROLL_UP(this, Mx, My);
						return;
					}
				case C_MSCROLL_DOWN:
					{
						if(ACTION_ON_SCROLL_DOWN)
							ACTION_ON_SCROLL_DOWN(this, Mx, My);
						return;
					}
				case C_SELECT:
					{
						if(ACTION_ON_SELECT)
							ACTION_ON_SELECT(this, Mx, My);
						return;
					}
				case C_UNSELECT:
					{
						if(ACTION_ON_UNSELECT)
							ACTION_ON_UNSELECT(this, Mx, My);
						return;
					}
			}
	}