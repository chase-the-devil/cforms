//#pragma once

#ifndef	__CFORMS__				//if not defined __CFORMS__ - its custom value, that i just created
								//this check is standart thing in headers, because they can be included several times
								//so now we can't include this header more then one time.
	#define __CFORMS__

	#ifdef UNICODE				//if defined to use unicode characters
		#undef UNICODE			//undefining it
	#endif

	#ifndef	MULTIBYTE					//if not defined to use multibyte characters
		#define MULTIBYTE				//defining to use it
	#endif

	#define  _CRT_SECURE_NO_WARNINGS	//only for Visual Studio 12 and higher, for not use warnings as errors (sometimes)

	#include <Windows.h>	//including Windows.h library, it contain all API base of Windows

//************************************************************************************************
//******** Defining constants
//************************************************************************************************


	//#if !defined SCREEN_WIDTH || !defined SCREEN_HEIGHT
		#define	 SCREEN_WIDTH			100			//max_normal = 195 - Its screen width constant wich will define
													//a width of rectangle, wich we will use as a screen in console																	
		#define  SCREEN_HEIGHT			25			//max_normal = 58 - Same thing but with height
	//#endif

	//These defines was made for save all window's and components borders sprites
	//You can check an ASCII table to look at them by their codes

	#define  WIN_HORIZONTAL				205
	#define  WIN_VERTICAL				186
	#define  WIN_TOP_LEFT				201
	#define  WIN_TOP_RIGHT				253
	#define  WIN_BOTTOM_LEFT			200
	#define  WIN_BOTTOM_RIGHT			18
	#define  WIN_BOTTOM_RIGHT_2_2		188

	#define  ARROW_PIX					219

	#define  COMP_HORIZONTAL_1			196
	#define  COMP_VERTICAL_1			179
	#define  COMP_HORIZONTAL_2			205
	#define  COMP_VERTICAL_2			186

	#define  COMP_TOP_LEFT_1_1			218
	#define  COMP_TOP_LEFT_1_2			214
	#define  COMP_TOP_LEFT_2_1			213

	#define  COMP_TOP_RIGHT_1_1			191
	#define	 COMP_TOP_RIGHT_1_2			183
	#define	 COMP_TOP_RIGHT_2_1			184

	#define  COMP_BOTTOM_LEFT_1_1		192
	#define  COMP_BOTTOM_LEFT_1_2		212
	#define  COMP_BOTTOM_LEFT_2_1		211

	#define  COMP_BOTTOM_RIGHT_1_1		217
	#define  COMP_BOTTOM_RIGHT_1_2		190
	#define	 COMP_BOTTOM_RIGHT_2_1		189

	#define  CARET_STYLE				95
	#define  CHECKBOX_CHECKED			88

	//This is defined constant by me, by experement method
	//All values that are smaller this constand are scroll up actions
	//in some context, you'll see it later

	#define	 SCROLL_DOWN				0xFF880000

	//codes of control key state while some control key pressed

	#define	 LARGE_LETTER_SHIFT			16
	#define  LARGE_LETTER_CAPS			128

	#define	 ARROW_KEY_SIMPLE			288
	#define  ARROW_KEY_CAPS				416
	#define	 ARROW_KEY_SHIFT			304
	#define  ARROW_KEY_SHIFT_CAPS		432
	

//************************************************************************************************
//******** Functions
//************************************************************************************************

	//These functions were made for easy converting integer numbers to a string and on the contrary

	int	  strtoint(char*);
	char* inttostr(int	);


//************************************************************************************************
//******** Structures
//************************************************************************************************

	//This structure contain all information about each pixel(symbol in console)

	struct CPIXEL
		{
			TCHAR		PIX;			//symbol
			unsigned	OWNER_TYPE;		//type of owner of this pixel (type can be for example Window, Button ... Look at enum COBJECT_TYPE)
			unsigned	OWNER_CODE;		//the code of owner	(code its unic number for each object)
			const void* hOWNER;			//pointer to owner
			CPIXEL*		NEXT_LAYER;		//pointer to a next layer of a screen buffer
			CPIXEL*		PREV_LAYER;		//pointer to a previous layer of a screen buffer
		};								//About These layers will be explained in TCScreen class.
	

	//This structure contain informatio about text pool caret

	struct CMEMO_CURSOR
		{
			char	 STYLE;		//wich symbol will sign the cursor
			int		 X;			//X coordinate
			int		 Y;			//Y coordinate
			int		 POS;		//position of cursor by number of symbols from start of a text
								//for example: if X = 2 and Y = 2 and width of text pool = 4 and height = 4
								//then POS = 12 (X and Y can be 0) 
			char	 PIX_UNDER;	//kind of char buffer, we need to know wich symbol was hidden by caret at a moment
		};

	//A structure for save information about each ListBox string

	struct CLIST_STRING
		{
			const char*	  STRING;			//A string value
			char		  SELECTED_STYLE;	//Symbol that will sign selected string
			char		  UNSELECTED_STYLE;	//Symbol that will sign unselected string
			unsigned	  OFFSET;			//Offset of string value from start of a ListBox string 
			bool		  SELECTED = false;	//Selection flag
			int			  INDEX = 0;		//Index of a string in a ListBox
			CLIST_STRING* NEXT = NULL;		//Pointer to next string
			CLIST_STRING* PREV = NULL;		//Pointer to previous string
		};

	//Almost the same structure as CLIST_STRING but it's destination tobring information about some CLIST_STRING
	//thta might need programmer, but he/she can't change some critical fields

	struct CLIST_STRING_INFO
		{
			const char**  STRING;			//A string value
			char*		  SELECTED_STYLE;	//Symbol that will sign selected string
			char*		  UNSELECTED_STYLE;	//Symbol that will sign unselected string
			unsigned*	  OFFSET;			//Offset of string value from start of a ListBox string
			int			  INDEX;			//Index of a string in a ListBox
		};									//Index can't be changed because it will be copyed from original

	//This is structure that contain information about SubMenu string

	struct CSUB_MENU_STRING
		{
			const char*		  STRING;							//A string value
			char			  SELECTED_STYLE;					//Symbol that will sign selected string
			char			  UNSELECTED_STYLE;					//Symbol that will sign unselected string
			unsigned		  OFFSET;							//Offset of string value from start of a SubMenu string
			bool			  SELECTED = false;					//Selection flag
			void			  (*ACTION_ON_CLICK) (void) = NULL;	//Pointer on a function that will be called after this string got clicked
			CSUB_MENU_STRING* NEXT = NULL;						//Pointer to a next string
			CSUB_MENU_STRING* PREV = NULL;						//Pointer to a previous string
		};

	//The same thig here with getting information about SubMenu string

	struct CSUB_MENU_STRING_INFO
		{
			const char**	  STRING;							//A string value
			char*			  SELECTED_STYLE;					//Symbol that will sign selected string
			char*			  UNSELECTED_STYLE;					//Symbol that will sign unselected string
			unsigned*		  OFFSET;							//Offset of string value from start of a SubMenu string
			void		  (**ACTION_ON_CLICK) (void);		//Pointer on a function that will be called after this string got clicked
		};

//************************************************************************************************
//******** Enums
//************************************************************************************************

	//So that is a set of objects types

	enum COBJECT_TYPE
		{
			CARROW = 1,		//Arrow = 1
			CWINDOW,		//Window = 2
			CBUTTON,		//Button = 3
			CLABEL,			//Label = 4
			CMEMO,			//Memo = 5
			CLISTBOX,		//ListBox = 6
			CCHECKBOX,		//CheckBox = 7
			CSUBMENU,		//SubMenu = 8
			CMAINMENU		//MainMenu = 9
		};  //could be continued with creating new objects ...

	//There values that sign draw event in Redraw functions

	enum DRAW_EVENT
		{
			DRAW,			//Just draw object
			REDRAW,			//Undraw object, then draw it at new place
			UNDRAW			//Undraw object
		};

	//There enum of all actions that could be cathed by this library

	enum C_EVENT
		{
			C_MMOVE,		//Mouse move
			C_MDOWNL,		//Mous down left button
			C_MDOWNR,		//Mouse down right button
			C_MUP,			//Mouse up
			C_MDBCLICK,		//Mouse double click
			C_MSCROLL_UP,	//Mouse scroll up
			C_MSCROLL_DOWN,	//Mouse scroll down
			C_KEY_INPUT,	//A key pressed/released event
			C_SELECT,		//Some object was selected
			C_UNSELECT		//Some object was unselected
		};

	//There values of position of MainMenu

	enum CMAIN_MENU_POSITION
		{
			C_TOP,			//Align to top border
			C_BOTTOM,		//Align to bottom border
			C_LEFT,			//Align to left border
			C_RIGHT			//Align to left border
		};

//************************************************************************************************
//******** Main classes
//************************************************************************************************

	class TCScreen
		{
			protected:
				bool				IN_DRAW;
				TCHAR				_LINE		 [SCREEN_WIDTH*SCREEN_HEIGHT];
				COORD				POS;
				DWORD				OFFSET;
			public:
				CONSOLE_CURSOR_INFO CI;
				CPIXEL*				FAKE_CBUFFER [SCREEN_WIDTH][SCREEN_HEIGHT];
									TCScreen	 (char SYMBOL);
				bool				REDRAW		 ();
		};

	class TCApplication
		{
			protected:
				unsigned			MAX_CODE;
				bool				TERMINATED = false;
				bool				STARTED = false;
				char				ARROW_STYLE;
				int					LOGO_DELAY;
			public:
				TCScreen*			MAIN_SCR;
				void*				ARROW;
				TCHAR				LOGO		   [SCREEN_WIDTH*SCREEN_HEIGHT];
									TCApplication  (char SCR_SYMBOL_IN);
									TCApplication  (char SCR_SYMBOL_IN, char** LOGO_SPRITE, int PAUSE);
									TCApplication  (char SCR_SYMBOL_IN, char ARROW_SYMBOL);
									TCApplication  (char SCR_SYMBOL_IN, char ARROW_SYMBOL, char** LOGO_SPRITE, int PAUSE);
									~TCApplication ();
				void				RUN			   ();
				unsigned			GET_MAX_CODE   ();
				bool				IN_PROCESS	   ();
		};

	class TCObject abstract
		{
			protected:
				unsigned	 	  CODE;
				unsigned	 	  TYPE;
				int			 	  X;
				int			 	  Y;
				int				  MAX_WIDTH;
				int				  MAX_HEIGHT;
				unsigned	 	  WIDTH;
				unsigned	 	  HEIGHT;
				char			  BACKGROUND;
				unsigned		  WDH_HGH_RET				(unsigned new_PARAM);
			public:
				CPIXEL***	 	  SPRITE = NULL;
				TCObject*		  MENU = NULL;
				bool			  ENABLED;
				int				  TAG;
				int				  AVAILABLE_XMIN;
				int				  AVAILABLE_YMIN;
				int				  AVAILABLE_XMAX;
				int				  AVAILABLE_YMAX;
				virtual bool 	  STATIC_REDRAW				 (bool is_UPDATE);
				virtual bool 	  DYNAMIC_REDRAW			 (int new_X, int new_Y, DRAW_EVENT in_EVENT, bool is_UPDATE);
				int				  GET_MAX_HEIGHT			 ();
				int				  GET_MAX_WIDTH				 ();
				unsigned	 	  GET_WIDTH					 ();
				unsigned	 	  GET_HEIGHT				 ();
				int				  GET_X						 ();
				int				  GET_Y						 ();
				unsigned		  GET_CODE					 ();
				unsigned		  GET_TYPE					 ();
				TCObject									 (unsigned new_WIDTH, unsigned new_HEIGHT, int new_X, int new_Y, char in_BACKGROUND, unsigned in_TYPE);
				~TCObject									 ();
				virtual void	  REACT						 (int Mx, int My, WORD ACTION_TYPE) abstract;
				void			  (*ACTION_ON_MOUSE_MOVE)	 (TCObject* SENDER, int Mx, int My);
				void			  (*ACTION_ON_MOUSE_DBCLICK) (TCObject* SENDER, int Mx, int My, DWORD BUTTON_PRESSED);
				void			  (*ACTION_ON_MOUSE_DOWN)	 (TCObject* SENDER, int Mx, int My);
				void			  (*ACTION_ON_MOUSE_DOWN_R)	 (TCObject* SENDER, int Mx, int My);
				void			  (*ACTION_ON_MOUSE_UP)		 (TCObject* SENDER, int Mx, int My, DWORD BUTTON_RELEASED);
				void			  (*ACTION_ON_SCROLL_UP)	 (TCObject* SENDER, int Mx, int My);
				void			  (*ACTION_ON_SCROLL_DOWN)	 (TCObject* SENDER, int Mx, int My);
				void			  (*ACTION_ON_SELECT)		 (TCObject* SENDER, int Mx, int My);
				void			  (*ACTION_ON_UNSELECT)		 (TCObject* SENDER, int Mx, int My);
		};

	class TCComponent abstract : public TCObject
		{
			protected:
				TCObject* PARENT = NULL;
			public:
				TCComponent				 (unsigned new_WIDTH, unsigned new_HEIGHT, int new_X, int new_Y, char in_BACKGROUND, unsigned in_TYPE, TCObject* in_PARENT);
				TCObject* GET_PARENT	 ();
				bool	  STATIC_REDRAW  (bool is_UPDATE) override;
				bool	  DYNAMIC_REDRAW (int, int, DRAW_EVENT, bool) override;
		};

//************************************************************************************************
//******** Child classes[TCObject]
//************************************************************************************************

	class TCArrow : public TCObject
		{
			public:
				TCObject*	 SELECTED_BEFORE = NULL;
				TCObject*	 STICKED		 = NULL;
				TCObject*	 RESIZED		 = NULL;
				TCComponent* CLICKED		 = NULL;
				TCObject*	 SELECTED		 = NULL;
				DWORD		 LAST_PRESSED	 = NULL;
				WORD		 INPUT_KEY;
				DWORD		 CONTROL_KEY_STATE;
								TCArrow		();
								TCArrow		(char STYLE);
				void		 REACT			(int, int, WORD);
				bool		 DYNAMIC_REDRAW (int new_X, int new_Y, DRAW_EVENT in_EVENT, bool is_UPDATE) override;
		};

	class TCSubMenu : public TCObject
		{
			protected:
				TCObject*			   PARENT = NULL;
				CSUB_MENU_STRING*	   FIRST = NULL;
				CSUB_MENU_STRING*	   SELECTED = NULL;
				int					   COUNT;
				void				   CHECK			 (int Mx, int My, WORD ACTION_TYPE);
				void				   UPDATE			 (bool SHOW);
				CSUB_MENU_STRING*	   GET_BY_INDEX		 (int index);
			public:
									   TCSubMenu		 (unsigned new_WIDTH, unsigned new_HEIGHT, char in_BACKGROUND, char in_SHADOW, TCObject* new_PARENT);
									   ~TCSubMenu		 ();
				void				   PUSH				 (const char* STR, char UNSEL, char SEL, unsigned OFFSET, void (*ACTION) ());
				void				   PUSH				 (const char* STR, char UNSEL, char SEL, unsigned OFFSET);
				void				   PUSH				 (const char* STR, char UNSEL, char SEL);
				void				   PUSH				 (char UNSEL, char SEL);
				void				   POP				 ();
				CSUB_MENU_STRING_INFO* GET_INFO_BY_INDEX (int index);
				CSUB_MENU_STRING_INFO* GET_SELECTED_INFO ();
				int					   GET_COUNT		 ();
				void				   REACT		     (int Mx, int My, WORD ACTION_TYPE) override;
		};

//************************************************************************************************
//******** Additional structure
//************************************************************************************************

	struct CMAIN_MENU_SECTION
		{
			TCSubMenu*			SECTION_MENU;
			char*				SECTION_NAME;
			char				BACKGROUND;
			int					INDEX;
			int					POS;
			CMAIN_MENU_SECTION*	NEXT = NULL;
			CMAIN_MENU_SECTION* PREV = NULL;
		};

//************************************************************************************************
//********		-=\}{/=-
//************************************************************************************************

	class TCMainMenu : public TCObject
		{
			protected:
				CMAIN_MENU_SECTION* FIRST = NULL;
				CMAIN_MENU_SECTION* LAST = NULL;
				CMAIN_MENU_SECTION* FIRST_DRAWED = NULL;
				int					COUNT;
				int					MENU_WIDTH;
				void				UPDATE				 (bool SHOW);
				bool				CHECK				 (int Mx, int My);
				CMAIN_MENU_SECTION* GET_SECTION_BY_INDEX (int index);
				void				RE_INDEX			 (CMAIN_MENU_SECTION* BUF, bool PLUS);
			public:
				CMAIN_MENU_POSITION ALIGN;
									TCMainMenu			 (CMAIN_MENU_POSITION in_ALIGN, int in_WIDTH, char in_BACKGROUND);
				void				MOVE_DOWN();
				void				MOVE_UP();
				void				INSERT				 (int index, TCSubMenu* in_MENU, char* NAME, char in_BACKGROUND, char in_SEL_BACKGROUND);
				TCSubMenu*			GET_BY_INDEX		 (int index);
				void				ADD_TO_START		 (TCSubMenu* in_MENU, char* NAME, char in_BACKGROUND);
				void				ADD_TO_END			 (TCSubMenu* in_MENU, char* NAME, char in_BACKGROUND);
				void				ERASE				 (int index);
				void				REACT				 (int Mx, int My, WORD ACTION_TYPE) override;
		};

	class TCWindow : public TCObject
		{
			public:
						  TCWindow					(unsigned new_WIDTH, unsigned new_HEIGHT, int new_X, int new_Y, char in_BACKGROUND);
				void	  RESIZE					(int new_WIDTH, int new_HEIGHT);
				void	  REACT						(int Mx, int My, WORD ACTION_TYPE) override;
				void	  (*ACTION_ON_START_RESIZE)	(TCObject* SENDER, int Mx, int My, unsigned new_WIDTH, unsigned new_HEIGHT);
		};

//************************************************************************************************
//******** Child classes[TCComponent]
//************************************************************************************************

	class TCButton : public TCComponent
		{
			protected:
				const char* CAPTION = NULL;
				int			CAP_X;
				int			CAP_Y;
				void		SP_INIT			 (char in_BACKGROUND, char in_SHADOW);
				void		SP_LOAD_PUSHED	 ();
				void		SP_LOAD_RELEASED ();
			public:
				char**		RELEASED_SP;
				char**		PUSHED_SP;
							TCButton		 (unsigned new_WIDTH, unsigned new_HEIGHT, int new_X, int new_Y, char in_BACKGROUND, char in_SHADOW, TCObject* in_PARENT);
							~TCButton		 ();
				void		REACT			 (int Mx, int My, WORD ACTION_TYPE) override;
				void		SET_CAPTION		 (const char* new_CAPTION, int x, int y);
				const char* GET_CAPTION		 ();
		};

	class TCLabel : public TCComponent
		{
			protected:
				const char* CAPTION = NULL;
				int			CAP_X;
				int			CAP_Y;
			public:
							TCLabel		(unsigned new_WIDTH, unsigned new_HEIGHT, int new_X, int new_Y, char in_BACKGROUND, const char* in_CAPTION, int in_CAP_X, int in_CAP_Y, TCObject* in_PARENT);
							TCLabel		(unsigned new_WIDTH, unsigned new_HEIGHT, int new_X, int new_Y, char in_BACKGROUND, TCObject* in_PARENT);
				void		REACT		(int Mx, int My, WORD ACTION_TYPE) override;
				void		SET_CAPTION (const char* new_CAPTION, int x, int y);
				const char* GET_CAPTION	();
		};

	class TCMemo : public TCComponent
		{
			protected:
				char**		 TEXT_BUFFER = NULL;
				int			 BUF_X;
				int			 BUF_Y;
				int	    	 BUF_WIDTH;
				int			 BUF_HEIGHT;
				int		     AVAILABLE_WIDTH;
				int			 AVAILABLE_HEIGHT;
				unsigned	 EOT;
				char		 SPACE;
				CMEMO_CURSOR CARET;
				char		 TO_ADDITIONAL		 (char SINGLE);
				void		 SELECT				 (bool SELECTED);
				void		 INPUT				 (WORD INPUT_SYMBOL, DWORD CONTROL_KEY, bool SHOW_CARET);
				void		 SHOW_BUFFER		 ();
			public:
				bool		 INSERT_MODE;
				void	 (*ACTION_ON_CHANGE) (TCObject* SENDER, int Mx, int My);
								TCMemo			 (unsigned new_WIDTH, unsigned new_HEIGHT, unsigned new_BUF_WIDTH, unsigned new_BUF_HEIGHT, int new_X, int new_Y, char in_BACKGROUND, TCObject* in_PARENT);
								~TCMemo			 ();
				void		 UPDATE				 ();
				void		 ADD_TEXT			 (const char* in_STR);
				char*		 GET_TEXT			 ();
				void		 CLEAR				 ();
				void		 SET_TEXT			 (const char* new_TEXT);
				void		 REACT				 (int Mx, int My, WORD ACTION_TYPE) override;
		};

	class TCListBox : public TCComponent
		{
			protected:
				CLIST_STRING* FIRST = NULL;
				CLIST_STRING* LAST = NULL;
				CLIST_STRING* SELECTED = NULL;
				CLIST_STRING* FIRST_DRAWED = NULL;
				int			  COUNT;
				void		  UPDATE_LIST			 ();
				void		  CHECK					 (int Mx, int My);
				CLIST_STRING* GET_BY_INDEX			 (int in_INDEX);
			public:
								TCListBox			 (unsigned new_WIDTH, unsigned new_HEIGHT, int new_X, int new_Y, char in_BACKGROUND, TCObject* in_PARENT);
								~TCListBox			 ();
				CLIST_STRING_INFO* GET_SELECTED_INFO ();
				CLIST_STRING_INFO* GET_INFO_BY_INDEX (int in_INDEX);
				int				   GET_COUNT		 ();
				void			   INSERT_AFTER		 (int, const char* in_STRING, char in_SELECTED_STYLE, char in_UNSELECTED_STYLE, unsigned in_OFFSET);
				void			   ERASE			 (int in_INDEX);
				void			   ADD_TO_END		 (const char* in_STRING, char in_SELECTED_STYLE, char in_UNSELECTED_STYLE, unsigned in_OFFSET);
				void			   ADD_TO_START		 (const char* in_STRING, char in_SELECTED_STYLE, char in_UNSELECTED_STYLE, unsigned in_OFFSET);
				void			   MOVE_UP			 ();
				void			   MOVE_DOWN		 ();
				void			   REACT			 (int Mx, int My, WORD ACTION_TYPE) override;
		};

	class TCCheckBox : public TCComponent
		{
			protected:
				bool CHECKED_STATUS;
			public:
						TCCheckBox(int new_X, int new_Y, char in_BACKGROUND, TCObject* in_PARENT);
						TCCheckBox(int new_X, int new_Y, char in_BACKGROUND, bool in_STATUS, TCObject* in_PARENT);
				bool CHECK(bool in_STATUS);
				bool CHECKED();
				void REACT(int Mx, int My, WORD) override;
		};

#endif //#ifndef __CFORMS__